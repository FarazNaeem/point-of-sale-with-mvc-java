/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllerclasses;

import java.awt.event.MouseEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Faraz Naeem
 */
public class accountcontroller {
    viewclasses.DashBoardViewClass view;
    modelclasses.accountmodel model;
    public accountcontroller(){
        
    }
    public accountcontroller(viewclasses.DashBoardViewClass view,modelclasses.accountmodel model){
        this.view=view;
        this.model=model;
        
    }
    public String getsystemcurrentdate(){
      DateTimeFormatter dtf= DateTimeFormatter.ofPattern("yyyy/MM/dd");
      LocalDateTime now = LocalDateTime.now();
      return dtf.format(now);
    }
    
}
