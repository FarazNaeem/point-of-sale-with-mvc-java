/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllerclasses;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JOptionPane;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Faraz Naeem
 */
public class searchcontroller {
   viewclasses.DashBoardViewClass view;
   modelclasses.databasemodel model;
   int itemcount=0,quant=1;
   Double itemamount=0.0;
   Double amount=0.0,discount=0.0,bill=0.0;
   Boolean flag;
   int newrowcount=-1;
   DefaultTableModel tablemodel= new DefaultTableModel();
   JTable table= new JTable(tablemodel);
   TableModelEvent tableevent;
   Vector salevector= new Vector();
   ArrayList salelist= new ArrayList();
   ArrayList<modelclasses.Product> saleLine;
   
    public searchcontroller(){

    }
    public searchcontroller(viewclasses.DashBoardViewClass view,modelclasses.databasemodel model){
        this.view=view;
        this.model=model;
        saleLine= new ArrayList<>();         
        table= view.gettable();

        bill=0.0;
        amount=0.0;
       view.setmenuitemlistener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                 //To change body of generated methods, choose Tools | Templates.
                 int opt=JOptionPane.showConfirmDialog(null,"Are You Sure To Update Software","Question",JOptionPane.YES_NO_OPTION);
                 if(opt==0){

                     Desktop d= Desktop.getDesktop();
                     try {
                         d.browse(new URI("https://github.com/Faizanf33/IMS/archive/v1.0.zip"));
                     } catch (URISyntaxException ex) {
                         Logger.getLogger(searchcontroller.class.getName()).log(Level.SEVERE, null, ex);
                     } catch (IOException ex) {
                         Logger.getLogger(searchcontroller.class.getName()).log(Level.SEVERE, null, ex);
                     }
                 }else{

                 }
            }
        });
       view.homesearchtabellistener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
                 //To change body of generated methods, choose Tools | Templates.
                 if(me.getClickCount()==2){
                     JTable target=(JTable)me.getSource();
                     int row=target.getSelectedRow();
                     int squant=Integer.parseInt(target.getValueAt(row, 5).toString());
                     
                     model.removeaddedsaleincart(row, squant);
                     DefaultTableModel tmodel=view.getDefaulttablemodel();
                     tmodel.removeRow(row);
                 }
            }

            @Override
            public void mousePressed(MouseEvent me) {
                
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            

            @Override
            public void mouseExited(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
            }
        });
       view.tablelistener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent tme) {
                
                int col=0;
                int row=0;
                switch(tme.getType()){
                    case TableModelEvent.DELETE:
                        try{
                            int rownum=table.getSelectedRow();
                            saleLine.remove(rownum);
                            tablemodel.setRowCount(0);
                            reloadSaleLine();
                            
                            quant=1;
                            bill=calculatebill();
                            printbill(bill,amount,itemcount);
                            bill=0.0;
                            amount=0.0;                            
                        }catch(Exception e){
                            JOptionPane.showMessageDialog(null,"error in deleting entry "+e);
                        }

                            
                            break;
                    case TableModelEvent.UPDATE:
                        JOptionPane.showMessageDialog(null,"aupdfjpupdpd");
                        row=table.getSelectedRow();
                        col=table.getSelectedColumn();
                         
                        String x=table.getValueAt(row, col).toString();
                        quant=Integer.parseInt(x);
                        int key=Integer.parseInt(table.getValueAt(row, 0).toString());

                        int salequant1=model.addinsalecart(key, quant);
                       
                        if(salequant1<=model.getremainingquantity() && salequant1!=0){
                        
                            bill=calculatebill();
                            printbill(bill,amount,itemcount);
                            bill=0.0;
                            amount=0.0;
                        }else{
                            table.setValueAt(model.getremainingquantity(), row, col);
                        }
                        
                        break;
                    case TableModelEvent.INSERT:
                            quant=1;
                            bill=calculatebill();
                            printbill(bill,amount,itemcount);
                            bill=0.0;
                            amount=0.0;                         
                        break;
                      
                }

            }
        });
       view.combolistener(new KeyAdapter() {
        @Override
        public void keyReleased(KeyEvent event) {
        if(event.getKeyChar()==KeyEvent.VK_ENTER){
            String x=view.getsearcheditem();
            
            if(!x.equals("")){
          
           flag= model.setsearcheditem(view.getsearcheditem());
                if(flag==true){
                    int rem=model.getremainingquantity();
                    int key=model.getid();

                    int salequant=model.addinsalecart(key,1);
                    
                    if(rem >= salequant &&  salequant!=0){
                     modelclasses.Product product= new modelclasses.Product();
                     product.setItemid(model.getid());
                     product.setItemname(model.getitemname());
                     product.setCompanyname(model.getcompanyname());
                     product.setPrice(model.getitemsaleprice());
                     product.setQuantity(model.getremainingquantity());
                     product.setSaletype(model.getitemsaletpye());
                     saleLine.add(product);
                     view.setid(model.getid());
                     view.setitemname(model.getitemname());
                     view.setcompanyname(model.getcompanyname());
                     view.setitemsaleprice(model.getitemsaleprice());
                     view.setitemsaletype(model.getitemsaletpye());
                     view.adddintable();
                    }else{
                        JOptionPane.showMessageDialog(null,"item { "+model.getitemname()+ " "+model.getcompanyname()+" } stock finished!!!");
                    }  
                      

                }else{
                }
            }else{
                view.showdatanotfounderror();
            }
        }
       }   
      });
       view.setdiscountallowedtftfocusout(new FocusListener() {
            @Override
                public void focusGained(FocusEvent fe) {

                }

                @Override
                public void focusLost(FocusEvent fe) {
                    discount=view.getdiscountallowed();
                    if(view.gettotalbill()<=discount){
                        JOptionPane.showMessageDialog(null, "Discount Allowed cannot be greater than Total Ammount(Bill)");
                        view.setdiscountallowedtft(0.0);
                    }else{
                    calculatebill();
                    printbill(bill,amount,itemcount);
                    bill=0.0;
                    amount=0.0;               
                    }
            }
        });
       view.discounttftlistener(new KeyAdapter() {
           @Override
            public void keyReleased(KeyEvent event) {
                if(event.getKeyChar()==KeyEvent.VK_ENTER){
                    discount=view.getdiscountallowed();
                    if(view.gettotalbill()<=discount){
                        JOptionPane.showMessageDialog(null, "Discount Allowed cannot be greater than Total Ammount(Bill)");
                        view.setdiscountallowedtft(0.0);
                    }else{
                    calculatebill();
                    printbill(bill,amount,itemcount);
                    bill=0.0;
                    amount=0.0;               
                    }

                }
            }
       });
       view.savebtnlistener(new Action() {
            
            @Override
            public void actionPerformed(ActionEvent ae) {
                //To change body of generated methods, choose Tools | Templates.
                saleLine.clear();
                tablemodel=view.getDefaulttablemodel();
                model.setsalebill(view.getbill());
                model.settotalitempurchased(view.gettotalitempurchased());
                model.setsalediscount(view.getdiscountallowed());
                model.setsaleday(getsystemday());
                model.setsaledate(getsystemdate());
                model.addnewsale();
//                model.setsalecartempty();
                for(int i=0;i<tablemodel.getRowCount();i++){
                    model.minussalequantity(Integer.parseInt(tablemodel.getValueAt(i, 0).toString()),Integer.parseInt(tablemodel.getValueAt(i, 5).toString()));                    
                }


                tablemodel.setRowCount(0);
                 
            }

            @Override
            public Object getValue(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void putValue(String string, Object o) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void setEnabled(boolean bln) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean isEnabled() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
       view.cancelbtnlistener(new Action() {
          @Override
          public Object getValue(String string) {
              throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
          }

          @Override
          public void putValue(String string, Object o) {
              throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
          }

          @Override
          public void setEnabled(boolean bln) {
              throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
          }

          @Override
          public boolean isEnabled() {
              throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
          }

          @Override
          public void addPropertyChangeListener(PropertyChangeListener pl) {
              throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
          }

          @Override
          public void removePropertyChangeListener(PropertyChangeListener pl) {
              throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
          }

          @Override
          public void actionPerformed(ActionEvent ae) {
               //To change body of generated methods, choose Tools | Templates.
               saleLine.clear();
               model.setsalecartempty();
               tablemodel=view.getDefaulttablemodel();
               tablemodel.setRowCount(0);
               view.setdiscountallowedtft(0.0);
               view.settotalamounttft(0.0);
               view.settotalbillft(0.0);
               view.settotalitemstft(0);
//               model.setsalecartempty();
               
               
          }
      });

       
    }
    public Double calculatebill(){
                try{
                   int x= table.getRowCount();
                   for(int i=0;i<x;i++){
                       int row=Integer.parseInt(table.getValueAt(i, 0).toString());
                       int id=Integer.parseInt(table.getValueAt(i, 0).toString());
                       String name=table.getValueAt(i, 1).toString();
                       String cname=table.getValueAt(i,2).toString();
                       Double sp= Double.parseDouble(table.getValueAt(i, 3).toString());
                       String type=table.getValueAt(i, 4).toString();
                       Double q = Double.parseDouble(table.getValueAt(i, 5).toString());
                       amount=amount+(sp*q);
                       bill=amount;
                       bill=bill-discount;
                   } 

                     
                }catch(NullPointerException ex){
                    JOptionPane.showMessageDialog(null,"index out pointer exception in bill" +ex);
                }
                return bill;
    }
    public double saleLineBilling(){
        double saleLinebill=0.0;
        for(modelclasses.Product prod : saleLine){
            saleLinebill=prod.getPrice()+saleLinebill;
        }
        if(saleLinebill > 0 && view.getdiscountallowed() > 0){
            saleLinebill=saleLinebill-view.getdiscountallowed();
        }
        return saleLinebill;
    }
    public void printbill(Double bill,Double amount,int itemcount){
                     view.settotalitemstft(table.getRowCount());
                     view.settotalamounttft(amount);
                     view.settotalbillft(bill);
    }
    public void reloadSaleLine(){
        for(modelclasses.Product prod : saleLine){
            view.setid(prod.getItemid());
            view.setitemname(prod.getItemname());
            view.setcompanyname(prod.getCompanyname());
            view.setitemsaleprice(prod.getPrice());
            view.setitemsaletype(prod.getSaletype());
            view.adddintable();
        }
    }
    public String getsystemday(){
        Date now = new Date();
 
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE"); // the day of the week abbreviated
        return simpleDateformat.format(now);
  }
    public String getsystemdate(){
      DateTimeFormatter dtf= DateTimeFormatter.ofPattern("yyyy/MM/dd");
      LocalDateTime now = LocalDateTime.now();
      return dtf.format(now);      
  }
}
