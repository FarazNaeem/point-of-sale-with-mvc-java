/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllerclasses;

import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;

/**
 *
 * @author Faraz Naeem
 */
public class deleteitemcontroller {
    viewclasses.DashBoardViewClass view;
    modelclasses.deleteitemsmodel model;
    boolean flag=false;
    public deleteitemcontroller(){
        
    }
    public deleteitemcontroller(modelclasses.deleteitemsmodel model,viewclasses.DashBoardViewClass view){
        this.model=model;
        this.view=view;
        view.deleteremovesearchcombolistener(new KeyAdapter() {
        @Override
            public void keyReleased(KeyEvent event) {
                if(event.getKeyChar()==KeyEvent.VK_ENTER){
                    String x=view.removegetsearchitem();
                    if(!x.equals("")){
                        flag=model.getsearchfromdb(x);
                        if(flag==true){
                                
                            
                            view.deletesetitemname(model.getitemname());
                            view.deletesetcompanyname(model.getcompanyname());
                            view.deletesetitemprice(model.getitemprice());
                            view.deletesetitemsaleprice(model.getitemsaleprice());
                            view.deletesetitemquantity(model.getitemquantity());
                            
                            view.showremovepanel();                            
                        }else{
                            
                        }
                    }else{
                        
                    }
                }
            }
        });
        view.deletecancelitembtnlistener(new Action() {
            @Override
            public Object getValue(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void putValue(String string, Object o) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void setEnabled(boolean bln) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean isEnabled() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void actionPerformed(ActionEvent ae) {
                 //To change body of generated methods, choose Tools | Templates.
                 view.hideremovepanel();
            }
        });
        view.deleteremoveitembtnlistener(new Action() {
            @Override
            public Object getValue(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void putValue(String string, Object o) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void setEnabled(boolean bln) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean isEnabled() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void actionPerformed(ActionEvent ae) {
                
                model.setitemname(view.removegetitemname());
                model.setcompanyname(view.removegetcompanyname());
                model.setitemprice(view.removegetitemprice());
                model.setitemsaleprice(view.removegetitemsaleprice());
                model.setitemquantity(view.removegetitemquantity());
                model.setitemsaletype(view.removegetitemsaletype());           
                model.deleteitem();                    
                

                view.hideremovepanel();
                //To change body of generated methods, choose Tools | Templates.
            }
        });
    }
    
    
}
