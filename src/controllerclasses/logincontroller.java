/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllerclasses;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.JOptionPane;
import viewclasses.DashBoardViewClass;

/**
 *
 * @author Faraz Naeem
 */
public class logincontroller {
   viewclasses.login loginview;
   modelclasses.loginmodel loginmodel;
   viewclasses.DashBoardViewClass view;
    public logincontroller(){
        
    }
    public logincontroller(viewclasses.login loginview,   modelclasses.loginmodel loginmodel,viewclasses.DashBoardViewClass view){
        this.loginview=loginview;
        this.loginmodel=loginmodel;
        this.view=view;
        loginview.loginbtnlistener(new Action() {
            @Override
            public Object getValue(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void putValue(String string, Object o) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void setEnabled(boolean bln) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean isEnabled() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void actionPerformed(ActionEvent ae) {
                 //To change body of generated methods, choose Tools | Templates.
                 loginmodel.setusername(loginview.getusername());
                 loginmodel.setpassword(loginview.getpassword());
                 boolean x=loginmodel.varifyuser();
                 if(x==true){
                        view.setVisible(true);
                        
                 }else{
                     JOptionPane.showMessageDialog(null,"Wrong Username or Password","Warning",JOptionPane.NO_OPTION);
                 }
            }
        });
    }
}
