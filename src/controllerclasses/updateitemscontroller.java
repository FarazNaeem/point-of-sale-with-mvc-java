/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllerclasses;

import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.JOptionPane;

/**
 *
 * @author Faraz Naeem
 */
public class updateitemscontroller {
    
    modelclasses.updateitemsmodel model;
    viewclasses.DashBoardViewClass view;
    boolean flag=false;
    public updateitemscontroller(){
        
    }
    
    public updateitemscontroller(modelclasses.updateitemsmodel model,viewclasses.DashBoardViewClass view){
        this.model=model;
        this.view=view;
        view.updatesavebtnlistener(new Action() {
            @Override
            public Object getValue(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void putValue(String string, Object o) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void setEnabled(boolean bln) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean isEnabled() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void actionPerformed(ActionEvent ae) {
                 //To change body of generated methods, choose Tools | Templates.
                 model.setitemname(view.updategetitemname());
                 model.setcompanyname(view.updategetcompanyname());
                 model.setitemsaleprice(view.updategetitemsaleprice());
                 model.setitemprice(view.updategetitemprice());
                 model.setitemsaletype(view.updategetitemsaletype());
                 model.setitemquantity(view.updategetitemquantity());
                 model.updateitem(view.updategetsearchitem());
                 view.additemsincombo(model.getitems());
                 view.hideudatepanel();
            }
        });
        view.updatecancelbtnlistener(new Action() {
            @Override
            public Object getValue(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void putValue(String string, Object o) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void setEnabled(boolean bln) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean isEnabled() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void actionPerformed(ActionEvent ae) {
                view.hideudatepanel(); //To change body of generated methods, choose Tools | Templates.
            }
        });
        view.updatesearchcomoblistener(new KeyAdapter() {
         @Override
            public void keyReleased(KeyEvent event) {
                if(event.getKeyChar()==KeyEvent.VK_ENTER){
                    String x=view.updategetsearchitem();
                    if(!x.equals("")){
                        flag=model.getsearchfromdb(x);
                        if(flag==true){
                            view.updatesetitemname(model.getitemname());
                            view.updatesetcompanyname(model.getcompanyname());
                            view.updatesetitemprice(model.getitemprice());
                            view.updatesetitemsaleprice(model.getitemsaleprice());
                            view.updatesetitemquantity(model.getitemquantity());
                            view.showupdatepanel();
                            
                        }else{
                            
                        }
                    }else{
                        
                    }
                }
            }
        });
        
    }
    
}
