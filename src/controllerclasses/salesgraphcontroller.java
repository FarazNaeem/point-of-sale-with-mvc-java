/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllerclasses;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.JOptionPane;

/**
 *
 * @author Faraz Naeem
 */
public class salesgraphcontroller {
    viewclasses.DashBoardViewClass view;
    modelclasses.salesgraphmodel model;
    public salesgraphcontroller(){
        
    }
    public salesgraphcontroller(viewclasses.DashBoardViewClass view,modelclasses.salesgraphmodel model){
        this.view=view;
        this.model=model;
        view.printgraphbtnlistener(new Action() {
            @Override
            public Object getValue(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void putValue(String string, Object o) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void setEnabled(boolean bln) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean isEnabled() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void actionPerformed(ActionEvent ae) {
                 //To change body of generated methods, choose Tools | Templates.

                    if(view.ismonthradioset()){
                     
                    String d=view.getgraphmonthdate();
                    String[] dd=d.split(",");
                    String date=dd[0]+"/"+dd[1];
                    
                    date=date+"/";
                    view.setsalegraphlist(model.getsalegraphsdata(date));
                 }else if(view.isyearradioset()){
                    String d=view.getgraphyeardate();
                    JOptionPane.showMessageDialog(null, d);
                    view.setsalegraphyearlist(model.getsalegraphyeardata(d));
                 }else{
                     JOptionPane.showMessageDialog(null,"Please check any Graph Type");
                 }
                     
                     
                 
                 
            }
        });
    }
    
    
    
}
