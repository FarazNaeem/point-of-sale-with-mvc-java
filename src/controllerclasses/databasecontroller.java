/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllerclasses;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;

import javax.swing.JOptionPane;

/**
 *
 * @author Faraz Naeem
 */
public class databasecontroller {
    modelclasses.databasemodel databasemodel;
    viewclasses.product product;
    viewclasses.DashBoardViewClass view;
    public databasecontroller(){
        
    }
    public databasecontroller(modelclasses.databasemodel databasemodel,viewclasses.DashBoardViewClass view,viewclasses.product p){
        this.databasemodel=databasemodel;
        this.view=view;
        this.product=p;
        view.addcancelbtnlistener(new Action() {
            @Override
            public Object getValue(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void putValue(String string, Object o) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void setEnabled(boolean bln) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean isEnabled() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void actionPerformed(ActionEvent ae) {
                view.hideadditemspanel(); //To change body of generated methods, choose Tools | Templates.
            }
        });
        view.additemsincombo(databasemodel.getitems());
        view.boton(new Action() {
            @Override
            public Object getValue(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void putValue(String string, Object o) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void setEnabled(boolean bln) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean isEnabled() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener pl) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void actionPerformed(ActionEvent ae) {
                int x;
                x=JOptionPane.showConfirmDialog(null, view.getitemname()+"\n"+view.getcompanyname()+"\n"+view.getitemprice()+"\n"+view.getitemsaleprice()+"\n"+view.getitemquantity()+"\n"+view.getitemsaletype()+"\n"+view.getitemaddeddate()+"\nsave");
                if(x==JOptionPane.YES_OPTION){
                    
                    databasemodel.setitemname(view.getitemname());
                    databasemodel.setcompanyname(view.getcompanyname());
                    databasemodel.setitemprice(view.getitemprice());
                    databasemodel.setitemsaleprice(view.getitemsaleprice());
                    databasemodel.setitemquantity(view.getitemquantity());
                    databasemodel.setitemaddeddate(view.getitemaddeddate());
                    databasemodel.setitemsaletype(view.getitemsaletype());
                    databasemodel.addnewitem();
                    view.additemsincombo(databasemodel.getitems());
                    view.resetadditems();
                    
                            
                }else{
                    view.showinputcancelerror();
                } //To change body of generated methods, choose Tools | Templates.
            }
        });
    }   
}

