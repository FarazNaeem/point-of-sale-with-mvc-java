/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelclasses;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Faraz Naeem
 */
public class updateitemsmodel {
    connection con;

    String itemname,companyname,itemsaletype;
    Double itemprice=0.0,itemsaleprice=0.0;
    int itemquantity=0,id,itemremainingquantity;
        public ArrayList<String> itemsadded= new ArrayList<String>();
    public updateitemsmodel(){
        
    }
    public updateitemsmodel(connection con){
        this.con=con;
    }
    public ArrayList getitems(){
        String qry="select * from items";
        try{
        con.rst=con.st.executeQuery(qry);
        while(con.rst.next()){
            itemname=con.rst.getString("item_name");
            companyname=con.rst.getString("company_name");
            id=con.rst.getInt("id");
           
            String prod=itemname+" "+companyname+" "+id;
            itemsadded.add(prod);
        }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error in fetching data from database "+ex);
        }
            
        return itemsadded;
    }
    public int getitemremainingquantity(){
        return itemremainingquantity;
    }
    public Double getitemprice(){
     return this.itemprice;   
    }
    public Double getitemsaleprice(){
        return this.itemsaleprice;
    }
    public int getitemquantity(){
        return this.itemquantity;
    }
    public String getitemname(){
     return this.itemname;   
    }
    public String getcompanyname(){
        return this.companyname;
    }
    public String getitemsaletype(){
        return this.itemsaletype;
    }
    public void setitemname(String name){
        this.itemname=name;
    }
    public void setcompanyname(String cname){
        this.companyname=cname;
    }
    public void setitemsaletype(String type){
        this.itemsaletype=type;
    }
    public void setitemprice(Double price){
        this.itemprice=price;
    }
    public void setitemsaleprice(Double sp){
        this.itemsaleprice=sp;
    }
    public void setitemquantity(int quant){
        this.itemquantity=quant;
    }
    public  boolean getsearchfromdb(String qry){
       String[] test= qry.split(" ");
       int size=test.length;
        if(test.length==1){
            String qry1="select items.id,items.item_name,items.company_name,item_info.itemsaleprice,item_info.itemprice,item_info.itemsaletype,item_info.remainingquantity from items left join item_info on items.id=item_info.itemid where items.id='"+test[0]+"'";
            try{
            con.rst=con.st.executeQuery(qry1);
            con.rst.next();
            itemname=con.rst.getString("item_name");
            companyname=con.rst.getString("company_name");
            itemsaleprice=con.rst.getDouble("itemsaleprice");
            itemsaletype=con.rst.getString("itemsaletype");
            itemprice=con.rst.getDouble("itemprice");
            itemquantity=con.rst.getInt("remainingquantity");
            return true;
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null,"error  fetching data from db"+ex);
                  return false;
            }
        }
        if(test.length>1){
            String qry1="select items.id,items.item_name,items.company_name,item_info.itemsaleprice,item_info.itemprice,item_info.itemsaletype,item_info.remainingquantity from items left join item_info on items.id=item_info.itemid where items.id='"+test[size-1]+"'";
            try{
            con.rst=con.st.executeQuery(qry1);
            con.rst.next();
            itemname=con.rst.getString("item_name");
            companyname=con.rst.getString("company_name");
            itemsaleprice=con.rst.getDouble("itemsaleprice");
            itemsaletype=con.rst.getString("itemsaletype");
            itemprice=con.rst.getDouble("itemprice");
            itemquantity=con.rst.getInt("remainingquantity");
            return true;
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null,"error  fetching data from db"+ex);
                  return false;
            }            
        }else{
            JOptionPane.showMessageDialog(null,"size error");
            return false;
        }
    }
     public void updateitem(String search){
         String[] searched=search.split(" ");
         int size=searched.length;
         if(size==1){
            
            try{
                String qry="update items set item_name='"+itemname+"' , company_name='"+companyname+"' where id='"+searched[0]+"'";
                String qry3="select * from items where id='"+searched[0]+"'";
          
                con.st.execute(qry);
                con.rst=con.st.executeQuery(qry3);
                con.rst.next();
                id=con.rst.getInt("id");

                String qry2="update item_info set itemquantity='"+itemquantity+"',remainingquantity='"+itemquantity+"',itemprice='"+itemprice+"',itemsaleprice='"+itemsaleprice+"',itemsaletype='"+itemsaletype+"' where itemid='"+searched[0]+"'";
                con.st.execute(qry2);

            }catch(Exception ex){
                    JOptionPane.showMessageDialog(null,"update error asdfsd"+ex);
            }
         }
         if(size>1){
            
            
            
            try{
                String qry="update items set item_name='"+itemname+"' , company_name='"+companyname+"' where id='"+searched[size-1]+"'";
            
                con.st.execute(qry);
                String qry3="select * from items where id='"+searched[size-1]+"'";
                con.rst=con.st.executeQuery(qry3);
                con.rst.next();
                id=con.rst.getInt("id");
                String qry2="update item_info set itemquantity='"+itemquantity+"',remainingquantity='"+itemquantity+"',itemprice='"+itemprice+"',itemsaleprice='"+itemsaleprice+"',itemsaletype='"+itemsaletype+"' where itemid='"+searched[size-1]+"'";
                con.st.execute(qry2);

            }catch(Exception ex){
                    JOptionPane.showMessageDialog(null,"update error fdsaf"+ex);
            }
         }else{
             
         }
     }
    
}
