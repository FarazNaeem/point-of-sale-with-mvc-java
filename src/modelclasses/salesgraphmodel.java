/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelclasses;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Faraz Naeem
 */
public class salesgraphmodel {
    connection con;
    ArrayList<String> list= new ArrayList<String>();
    public salesgraphmodel(){
        
    }
    public salesgraphmodel(connection con){
        this.con=con;
    }

    public ArrayList getsalegraphyeardata(String date){
        Double bill=0.0;
        try{
            String date2=date;
            for(int i=1;i<6;i++){
            if(i>1){
            int d=Integer.parseInt(date);
            d+=i;
            date=d+"";
            }    
            String qry="select sum(bill) as bill from sales where saledate like '"+date+"%'";
            
            con.rst=con.st.executeQuery(qry);
            if(con.rst.next()){
                bill=con.rst.getDouble("bill");
                list.add(i-1,bill+"");

            }
                date=date2;
            }
            return list;
        }catch (Exception ex){
            JOptionPane.showMessageDialog(null,"year fetching erro "+ex);
        }

        return null;
    }
    public ArrayList getsalegraphsdata(String date){
        Double bill=0.0;
        String date2=date;
        try{
            for(int i=1;i<32;i++){
                if(i<10){
                    date=date+"0"+i;
                }else{
                date=date+i;
                    
                }
                
            String qry="select sum(bill) as bill from sales where saledate='"+date+"'";
            con.rst=con.st.executeQuery(qry);
                if(con.rst.next()){
                    bill=con.rst.getDouble("bill");
                    list.add(i-1,bill+"");
                    
                }
                date=date2;
            }
            return list;
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"bill sum exception "+ex);
        }
        return null;
    }
    
}
