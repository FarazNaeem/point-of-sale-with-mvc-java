/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelclasses;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Faraz Naeem
 */
public class databasemodel {

    connection con;
    String itemname,companyname,saletype;
    Double itemprice,itemsaleprice;
    int quantity;
    int itemremainingquantity;
    String date, day;
    int id=0,totalitemspurchased=0;
    Double bill=0.0,discount=0.0;
    public ArrayList<String> itemsadded= new ArrayList<String>();
    public ArrayList<String> salecart= new ArrayList<String>();

    public ArrayList<Vector> salesonholdvector= new ArrayList<Vector>();
    public databasemodel(){
       
    }
    public databasemodel(connection c){
             this.con=c;
             for(int i=0;i<20;i++){
                 salecart.add(i, 0+"");
             }
    }

    

    
    
    
    public void setsalecartempty(){
        salecart.clear();
        
    }
    
    public int addinsalecart(int key,int squant){
        try{
            
        
        if(salecart.isEmpty()){
            salecart.add(key, squant+"");            
        }else{
            int x=Integer.parseInt(salecart.get(key));
            x=x+squant;
            salecart.add(key, x+"");
            return x;
        }
        }catch(ArrayIndexOutOfBoundsException ex){
            JOptionPane.showMessageDialog(null, "index out of bound addinsalecart "+ex);
        }
        return 0;
    }
    public int removefromsalecard(int key,int squant){
       if(salecart.isEmpty()){
           return 0;
       }
       salecart.remove(key);
       return 0;
    }
    public int removeaddedsaleincart(int key,int squant){
        if(salecart.isEmpty()){
            return 0;            
        }
        int x=Integer.parseInt(salecart.get(key));
        x=x-squant;
        salecart.add(key, x+"");
        return 0;
    }
    
    public void addnewitem(){
        
    String qry="insert into items(item_name,company_name) values('"+itemname+"','"+companyname+"')";
    String qry2="select * from items order by id desc limit 1";

            try {

                con.st.execute(qry);
                con.rst=con.st.executeQuery(qry2);                
                con.rst.next();            
                try{

                id=con.rst.getInt("id");
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null,"query 2"+ex);
                }
                String qry3="insert into item_info() values('"+id+"','"+quantity+"','"+itemprice+"','"+itemsaleprice+"','"+saletype+"','"+date+"','"+quantity+"')";
                con.st.execute(qry3);

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null,"query error "+ex);
                Logger.getLogger(databasemodel.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        
    }
    
    public void setitemname(String name){
        this.itemname=name;
    }
    public void setcompanyname(String companyname){
        this.companyname=companyname;
    }
    public void setitemprice(Double price){
        this.itemprice=price;
    } 
            
    public void setitemsaleprice(Double saleprice){
        this.itemsaleprice=saleprice;
    }
    public void setitemquantity(int quant){
        this.quantity=quant;
    }
    public void setitemsaletype(String saletype){
        this.saletype=saletype;
    }
    public void setitemaddeddate(String d){
        this.date=d;
    }

    public ArrayList getitems(){
        String qry="select * from items";
        try{
        con.rst=con.st.executeQuery(qry);
        while(con.rst.next()){
            id=con.rst.getInt("id");
            itemname=con.rst.getString("item_name");
            companyname=con.rst.getString("company_name");
           
            String prod=itemname+" "+companyname+" "+id;
            itemsadded.add(prod);
        }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error in fetching data from database "+ex);
        }
            
        return itemsadded;
    }
    public  boolean setsearcheditem(String qry){
       String[] test=qry.split(" "); 
        int size=test.length;

        if(test.length==1){
            String qry1="select items.id,items.item_name,items.company_name,item_info.itemsaleprice,item_info.itemsaletype,item_info.remainingquantity from items left join item_info on items.id=item_info.itemid where items.id='"+test[size-1]+"'";
            try{
            con.rst=con.st.executeQuery(qry1);
            con.rst.next();
            id=con.rst.getInt("id");
            itemname=con.rst.getString("item_name");
            companyname=con.rst.getString("company_name");
            itemsaleprice=con.rst.getDouble("itemsaleprice");
            saletype=con.rst.getString("itemsaletype");
            itemremainingquantity=con.rst.getInt("remainingquantity");
            return true;
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null,"error  fetching data from db"+ex);
                  return false;
            }
        }
        if(test.length>1){
            String qry1="select items.id,items.item_name,items.company_name,item_info.itemsaleprice,item_info.itemsaletype,item_info.remainingquantity from items left join item_info on items.id=item_info.itemid where items.id='"+test[size-1]+"'";
            try{
            con.rst=con.st.executeQuery(qry1);
            con.rst.next();
            id=con.rst.getInt("id");
            itemname=con.rst.getString("item_name");
            companyname=con.rst.getString("company_name");
            itemsaleprice=con.rst.getDouble("itemsaleprice");
            saletype=con.rst.getString("itemsaletype");
            itemremainingquantity=con.rst.getInt("remainingquantity");
            return true;
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null,"No Match Found"+ex);
                  return false;
            }            
        }else{
            JOptionPane.showMessageDialog(null,"ZERO match");
            return false;
        }
    }
    public int getid(){
        return id;
    }
    public String getitemname(){
        return itemname;
    }
    public String getcompanyname(){
        return companyname;
    }
    public Double getitemsaleprice(){
        return itemsaleprice;
    }
    public String getitemsaletpye(){
        return saletype;
    }
    public int getremainingquantity(){
        return itemremainingquantity;
    }

     public void minussalequantity(int id,int salequant){
        String qry="update item_info set remainingquantity = remainingquantity - '"+salequant+"'  where itemid='"+id+"'";
        try{
             boolean r=con.st.execute(qry);
            
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"updating quantity error "+ex);
        }
        
    }
    public void addnewsale() {
        String qry="insert into sales(bill,discount,saleday,saledate) values('"+bill+"','"+discount+"','"+day+"','"+date+"')";
        try{
            con.st.execute(qry);            
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"adding sale error "+ex);
        }

    }   
    public void setsalebill(Double bill){
        this.bill=bill;
    }
    public void setsalediscount(Double discount){
        this.discount=discount;
    }
    public void settotalitempurchased(int totalitempurchased){
        this.totalitemspurchased=totalitempurchased;
    }
    public void setsaleday(String day){
        this.day=day;
    }
    public void setsaledate(String date){
        this.date=date;
    }
}
