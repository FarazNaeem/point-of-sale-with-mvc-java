/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelclasses;

import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 *
 * @author Faraz Naeem
 */
public class stockmodel {
    connection con;
    int id,quantity;
    String itemname,companyname,itemsaletype;
    Double itemsaleprice;
    ArrayList<String> list= new ArrayList<String>();
    Vector v= new Vector();
    public stockmodel(){

    }
    public stockmodel(connection con){
        this.con=con;
    }
    public ArrayList getalldata(){
        
        try{
        String qry="select items.id,items.item_name,items.company_name,item_info.itemsaleprice,item_info.itemsaletype,item_info.remainingquantity from items left join item_info on items.id=item_info.itemid";
        con.rst=con.st.executeQuery(qry);
        while(con.rst.next()){
            id=con.rst.getInt("id");
            itemname=con.rst.getString("item_name");
            companyname=con.rst.getString("company_name");
            itemsaletype=con.rst.getString("itemsaletype");
            itemsaleprice=con.rst.getDouble("itemsaleprice");
            quantity=con.rst.getInt("remainingquantity");
            
            list.add(id+"");
            list.add(itemname);
            list.add(companyname);
            list.add(itemsaleprice+"");
            list.add(itemsaletype);
            list.add(quantity+"");
            

            

        }

        return list;
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"exception fetchall data");
        }
        return list;
    }
    public void cleardatavector(){
        
        list.clear();
    }
            
    
}
