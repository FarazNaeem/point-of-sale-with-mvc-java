/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelclasses;

import javax.swing.JOptionPane;

/**
 *
 * @author Faraz Naeem
 */
public class accountmodel {
    connection con;
    String date;
    public accountmodel(){
        
    }
    public accountmodel(connection con){
        this.con=con;
    }
    public Double updatetodaysale(){
        try{
            date=getdate();
            String qry="select sum(bill) as todaysale from sales where saledate='"+date+"'";
            con.rst=con.st.executeQuery(qry);
            con.rst.next();
            return con.rst.getDouble("todaysale");
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error fetching today sale"+ex);
        }
        return 0.0;
    }
    public String getdate(){
        return "";
    }
    public void setdate(String date){
        this.date=date;
    }
}
