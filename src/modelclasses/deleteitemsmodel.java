/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelclasses;

import javax.swing.JOptionPane;


/**
 *
 * @author Faraz Naeem
 */
public class deleteitemsmodel {
    connection con;
    String itemname,companyname,itemsaletype;
    Double itemprice,itemsaleprice;
    int itemquantity,id=0;
    public deleteitemsmodel(){
        
    }
    public deleteitemsmodel(connection con){
        this.con=con;
    }
    
    public Double getitemprice(){
     return this.itemprice;   
    }
    public Double getitemsaleprice(){
        return this.itemsaleprice;
    }
    public int getitemquantity(){
        return this.itemquantity;
    }
    public String getitemname(){
     return this.itemname;   
    }
    public String getcompanyname(){
        return this.companyname;
    }
    public String getitemsaletype(){
        return this.itemsaletype;
    }
    public void setitemname(String name){
        this.itemname=name;
    }
    public void setcompanyname(String cname){
        this.companyname=cname;
    }
    public void setitemsaletype(String type){
        this.itemsaletype=type;
    }
    public void setitemprice(Double price){
        this.itemprice=price;
    }
    public void setitemsaleprice(Double sp){
        this.itemsaleprice=sp;
    }
    public void setitemquantity(int quant){
        this.itemquantity=quant;
    }
    public void deleteitem(){
        String qry="select * from items where item_name='"+itemname+"' and company_name='"+companyname+"'";
        

        
        try{
            con.rst=con.st.executeQuery(qry);

            if(con.rst.next()){

              id=con.rst.getInt("id");
              String qry2="delete from item_info where itemid= '"+id+"'";
              String qry3="delete from items where id='"+id+"'";
              con.st.execute(qry2);
              con.st.execute(qry3);


         }else{
            JOptionPane.showMessageDialog(null, "empty resultset");  
        }  
                        
            
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "error deleting itesm fdfasf" +ex);
        }
    }
    public  boolean getsearchfromdb(String qry){
       String[] test= qry.split(" ");
        int size=test.length;
        if(size==1){
            String qry1="select items.id,items.item_name,items.company_name,item_info.itemsaleprice,item_info.itemprice,item_info.itemsaletype,item_info.remainingquantity from items left join item_info on items.id=item_info.itemid where items.id='"+test[0]+"'";
            try{
            con.rst=con.st.executeQuery(qry1);
            con.rst.next();
            itemname=con.rst.getString("item_name");
            companyname=con.rst.getString("company_name");
            itemsaleprice=con.rst.getDouble("itemsaleprice");
            itemsaletype=con.rst.getString("itemsaletype");
            itemprice=con.rst.getDouble("itemprice");
            itemquantity=con.rst.getInt("remainingquantity");
            return true;
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null,"error  fetching data from db"+ex);
                  return false;
            }
        }else if(test.length>1){
            String qry1="select items.id,items.item_name,items.company_name,item_info.itemsaleprice,item_info.itemprice,item_info.itemsaletype,item_info.remainingquantity from items left join item_info on items.id=item_info.itemid where items.id='"+test[size-1]+"'";
            try{
            con.rst=con.st.executeQuery(qry1);
            con.rst.next();
            itemname=con.rst.getString("item_name");
            companyname=con.rst.getString("company_name");
            itemsaleprice=con.rst.getDouble("itemsaleprice");
            itemsaletype=con.rst.getString("itemsaletype");
            itemprice=con.rst.getDouble("itemprice");
            itemquantity=con.rst.getInt("remainingquantity");
            return true;
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null,"error  fetching data from db"+ex);
                  return false;
            }            
        }else{
            JOptionPane.showMessageDialog(null,"size error");
            return false;
        }

}
}
