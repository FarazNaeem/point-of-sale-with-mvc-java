/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelclasses;

import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Faraz Naeem
 */
public class salesmodel {
    Double bill=0.0,discount=0.0;
    int totalitemspurchased=0;
    String day,date;
    connection con;
    public salesmodel(){
        
    }
    public salesmodel(connection con){
        this.con=con;
    }
    public void setsalebill(Double bill){
        this.bill=bill;
    }
    public void setsalediscount(Double discount){
        this.discount=discount;
    }
    public void settotalitempurchased(int totalitempurchased){
        this.totalitemspurchased=totalitempurchased;
    }
    public void setsaleday(String day){
        this.day=day;
    }
    public void setsaledate(String date){
        this.date=date;
    }
    public void addnewsale() {
        String qry="insert into sales(bill,discount,saleday,saledate) values('"+bill+"','"+discount+"','"+day+"','"+date+"')";
        try{
            con.st.execute(qry);            
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"adding sale error "+ex);
        }

    }
    public void minussalequantity(int id,int salequant){
        String qry="update item_info set remainingquantity = remainingquantity - '"+salequant+"'  where itemid='"+id+"'";
        try{
             boolean r=con.st.execute(qry);
            
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"updating quantity error "+ex);
        }
        
    }
    
}
