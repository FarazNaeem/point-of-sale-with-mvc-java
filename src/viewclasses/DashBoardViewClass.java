package viewclasses;


import java.awt.Color;
import javax.swing.border.EmptyBorder;

import javax.swing.border.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.awt.geom.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.JTextComponent;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Faraz Naeem
 */

class TextBubbleBorder extends AbstractBorder {

    private Color color;
    private int thickness = 4;
    private int radii = 8;
    private int pointerSize = 7;
    private Insets insets = null;
    private BasicStroke stroke = null;
    private int strokePad;
    private int pointerPad = 4;
    private boolean left = true;
    RenderingHints hints;

    TextBubbleBorder(Color color) {
        this(color, 4, 8, 7);
    }

    TextBubbleBorder(Color color, int thickness, int radii, int pointerSize) {
        this.thickness = thickness;
        this.radii = radii;
        this.pointerSize = pointerSize;
        this.color = color;

        stroke = new BasicStroke(thickness);
        strokePad = thickness / 2;

        hints = new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int pad = radii + strokePad;
        int bottomPad = pad + pointerSize + strokePad;
        insets = new Insets(pad, pad, bottomPad, pad);
    }

    TextBubbleBorder(
            Color color, int thickness, int radii, int pointerSize, boolean left) {
        this(color, thickness, radii, pointerSize);
        this.left = left;
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return insets;
    }

    @Override
    public Insets getBorderInsets(Component c, Insets insets) {
        return getBorderInsets(c);
    }

    @Override
    public void paintBorder(
            Component c,
            Graphics g,
            int x, int y,
            int width, int height) {

        Graphics2D g2 = (Graphics2D) g;

        int bottomLineY = height - thickness - pointerSize;

        RoundRectangle2D.Double bubble = new RoundRectangle2D.Double(
                0 + strokePad,
                0 + strokePad,
                width - thickness,
                bottomLineY,
                radii,
                radii);

        Polygon pointer = new Polygon();

        if (left) {
            // left point
            pointer.addPoint(
                    strokePad + radii + pointerPad,
                    bottomLineY);
            // right point
            pointer.addPoint(
                    strokePad + radii + pointerPad + pointerSize,
                    bottomLineY);
            // bottom point
            pointer.addPoint(
                    strokePad + radii + pointerPad + (pointerSize / 2),
                    height - strokePad);
        } else {
            // left point
            pointer.addPoint(
                    width - (strokePad + radii + pointerPad),
                    bottomLineY);
            // right point
            pointer.addPoint(
                    width - (strokePad + radii + pointerPad + pointerSize),
                    bottomLineY);
            // bottom point
            pointer.addPoint(
                    width - (strokePad + radii + pointerPad + (pointerSize / 2)),
                    height - strokePad);
        }

        Area area = new Area(bubble);
        area.add(new Area(pointer));

        g2.setRenderingHints(hints);

        // Paint the BG color of the parent, everywhere outside the clip
        // of the text bubble.
        Component parent  = c.getParent();
        if (parent!=null) {
            Color bg = parent.getBackground();
            Rectangle rect = new Rectangle(0,0,width, height);
            Area borderRegion = new Area(rect);
            borderRegion.subtract(area);
            g2.setClip(borderRegion);
            g2.setColor(bg);
            g2.fillRect(0, 0, width, height);
            g2.setClip(null);
        }

        g2.setColor(color);
        g2.setStroke(stroke);
        g2.draw(area);
    }
}
public class DashBoardViewClass extends javax.swing.JFrame {

    /**
     * Creates new form DashBoardViewClass
     */
    int saleid=0;
    public static JMenu jmenu;
    private Color foregroundcolor;
    private Font fontsize,newfontsize;
    private controllerclasses.databasecontroller controller=new controllerclasses.databasecontroller();
    int id,itemquantity;
    Double itemsaleprice;
    String itemname,companyname,itemsaletype;
    DefaultTableModel model;
    public ActionListener jmenuitemlistener;
    public JMenuItem menuItem;
    public JMenu systemmenu;
    public ButtonGroup groupradiobtn;
    ArrayList<String> list;
    DefaultTableModel stockTmodel;
    private static DecimalFormat doubleformater = new DecimalFormat("#.##");
    public DashBoardViewClass() {
        initComponents();
        this.setResizable(true);
        startthread();
       // model= (DefaultTableModel)combosearch.getModel();
        Dimension dem= Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dem.width/2-this.getSize().width/2,dem.height/2-this.getSize().height/2);
        
        
        systemmenu=systemjmenu1;
        leftpanel.setBorder(new EmptyBorder(10,10,10,10));
        
        AbstractBorder roundborder = new TextBubbleBorder(Color.white,4,16,0,false);
        leftpanel.setBorder(roundborder);
        jPanel3.setBorder(roundborder);
        billingpanel.setBorder(roundborder);
        proprietername.setBorder(roundborder);
        billingtoppanel.setBorder(roundborder);
        savebtn.setBorder(roundborder);
        cancelbtn.setBorder(roundborder);
        poslabel.setBorder(roundborder);
        discountallowedtft.setText("0.0");
        
        homesearchtable.getTableHeader().setOpaque(false);
        homesearchtable.getTableHeader().setBackground(new Color(1,50,67));
        homesearchtable.getTableHeader().setForeground(new Color(228,241,254));
    
        stocktable.getTableHeader().setOpaque(false);
        stocktable.getTableHeader().setBackground(new Color(1,50,67));
        stocktable.getTableHeader().setForeground(new Color(228,241,254));
    
        newfontsize= new Font("Tahoma",Font.BOLD,24);
        fontsize=new Font("Tahoma",Font.BOLD,18);
        foregroundcolor=homelabel.getForeground();
        
        model=(DefaultTableModel) homesearchtable.getModel();
        stockTmodel=(DefaultTableModel)stocktable.getModel();
        
        accountsearchpanel.setBorder(roundborder);
        
        updatesearchpanel.setBorder(roundborder);
        updateitemspanel1.setBorder(roundborder);

        updatepanelitemnamepanel.setBorder(roundborder);
        updatepanelcompanynamepanel.setBorder(roundborder);
        updatepanelitempricepanel.setBorder(roundborder);
        updatepanelitemsalepricepanel.setBorder(roundborder);
        updatepanelitemquantitypanel.setBorder(roundborder);
        updatepanelitemsaletypepanel.setBorder(roundborder);
        updatepanelupdatebtn.setBorder(roundborder);
        updatepanelcancelbtn.setBorder(roundborder);
        
        additemspanel.setBorder(roundborder);
        additemspanel2.setBorder(roundborder);
        updatepanelitemnamepanel1.setBorder(roundborder);
        updatepanelcompanynamepanel1.setBorder(roundborder);
        updatepanelitempricepanel1.setBorder(roundborder);
        updatepanelitemsalepricepanel1.setBorder(roundborder);
        updatepanelitemquantitypanel1.setBorder(roundborder);
        updatepanelitemsaletypepanel1.setBorder(roundborder);
        saveadditembtn.setBorder(roundborder);
        canceladditembtn.setBorder(roundborder);
        
        //delete items
        removeitemspanel2.setBorder(roundborder);
        updatepanelitemnamepanel2.setBorder(roundborder);
        updatepanelcompanynamepanel2.setBorder(roundborder);
        updatepanelitempricepanel2.setBorder(roundborder);
        updatepanelitemsalepricepanel2.setBorder(roundborder);
        updatepanelitemquantitypanel2.setBorder(roundborder);
        updatepanelitemsaletypepanel2.setBorder(roundborder);
        updatesearchpanel1.setBorder(roundborder);
        removeitembtn.setBorder(roundborder);
        removecancelbtn.setBorder(roundborder);
        
            stockheaderpanel.setBorder(roundborder);
        addstarlabel.setVisible(false);
        updatestarlabel.setVisible(false);
        removestarlabel.setVisible(false);
        stockstarlabel.setVisible(false);
        salegraphstarlabel.setVisible(false);
        accountstarlabel.setVisible(false);
        
        centerpanel.removeAll();
        centerpanel.add(homepanel);
        centerpanel.repaint();
        centerpanel.revalidate();
        
        salegraphheaderpanel.setBorder(roundborder);
        printgraphbtn.setBorder(roundborder);
        graphheaderpanel.setBorder(roundborder);
        addmenuitem();
        groupradiobtn= new ButtonGroup();
        groupradiobtn.add(monthradio);
        groupradiobtn.add(yearradio);

       UIManager UI=new UIManager();
       UI.put("OptionPane.background", Color.WHITE);
       UI.put("OptionPane.titlePane.background", Color.black);
       UI.put("Panel.background", Color.WHITE); 
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        parentpanel = new javax.swing.JPanel();
        leftpanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        homelabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        additemslabel = new javax.swing.JLabel();
        updateitemslabel = new javax.swing.JLabel();
        removeitemslabel = new javax.swing.JLabel();
        homestarlabel = new javax.swing.JLabel();
        addstarlabel = new javax.swing.JLabel();
        removestarlabel = new javax.swing.JLabel();
        updatestarlabel = new javax.swing.JLabel();
        accountlabel = new javax.swing.JLabel();
        stocklabel = new javax.swing.JLabel();
        accountstarlabel = new javax.swing.JLabel();
        stockstarlabel = new javax.swing.JLabel();
        salegraphlabel = new javax.swing.JLabel();
        salegraphstarlabel = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        shopnamepanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        poslabel = new javax.swing.JLabel();
        rightpanel = new javax.swing.JPanel();
        billingpanel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        proprietername = new javax.swing.JLabel();
        billingtoppanel = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        totalitemstft = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        totalamounttft = new javax.swing.JTextField();
        discountallowedtft = new javax.swing.JTextField();
        billtft = new javax.swing.JTextField();
        savebtn = new javax.swing.JButton();
        cancelbtn = new javax.swing.JButton();
        centerpanel = new javax.swing.JPanel();
        salegraphpanel = new javax.swing.JPanel();
        salegraphheaderpanel = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        graphheaderpanel = new javax.swing.JPanel();
        monthradio = new javax.swing.JRadioButton();
        yearradio = new javax.swing.JRadioButton();
        datechooserofmonth = new com.toedter.calendar.JDateChooser();
        datechooseryear = new com.toedter.calendar.JDateChooser();
        printgraphbtn = new javax.swing.JButton();
        stockpanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        stocktable = new javax.swing.JTable()
        {
            @Override
            public Component prepareRenderer (TableCellRenderer renderer, int rowIndex, int columnIndex){
                Component componenet = super.prepareRenderer(renderer, rowIndex, columnIndex);

                Object value = getModel().getValueAt(rowIndex,columnIndex);

                if(columnIndex == 5){

                    if(value.equals("5")||value.equals("4")||value.equals("3")||value.equals("2")||value.equals("1"))
                    {

                        componenet.setBackground(Color.YELLOW);
                        componenet.setForeground(Color.BLACK);

                    }
                    if(value.equals("0")){
                        componenet.setBackground(Color.RED);
                        componenet.setForeground(Color.BLACK);
                    }
                }
                else {
                    componenet.setBackground(Color.WHITE);
                    componenet.setForeground(Color.BLACK);
                }
                return componenet;
            }
        }
        ;
        stockheaderpanel = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        totalstocktft = new javax.swing.JTextField();
        showstockbtn = new javax.swing.JButton();
        cleartotalstocktable = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        accountpanel = new javax.swing.JPanel();
        accountsearchpanel = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        accountsearchcombo = new javax.swing.JComboBox<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        homepanel = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        combosearch = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        homesearchtable = new javax.swing.JTable();
        additems = new javax.swing.JPanel();
        additemspanel2 = new javax.swing.JPanel();
        updatepanelitemnamepanel1 = new javax.swing.JPanel();
        updateitemnamelabel1 = new javax.swing.JLabel();
        itemnametft = new javax.swing.JTextField();
        updatepanelcompanynamepanel1 = new javax.swing.JPanel();
        updatecompanynamepanel1 = new javax.swing.JLabel();
        companynametft = new javax.swing.JTextField();
        updatepanelitempricepanel1 = new javax.swing.JPanel();
        updateitempricelabel1 = new javax.swing.JLabel();
        itempricetft = new javax.swing.JTextField();
        updatepanelitemsalepricepanel1 = new javax.swing.JPanel();
        updateitemsalepricelabel1 = new javax.swing.JLabel();
        itemsalepricetft = new javax.swing.JTextField();
        updatepanelitemquantitypanel1 = new javax.swing.JPanel();
        updateitemquantitylabel1 = new javax.swing.JLabel();
        itemquantitytft = new javax.swing.JTextField();
        updatepanelitemsaletypepanel1 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        itemsaletypecombo = new javax.swing.JComboBox<>();
        saveadditembtn = new javax.swing.JButton();
        canceladditembtn = new javax.swing.JButton();
        additemspanel = new javax.swing.JPanel();
        additemspanellabel = new javax.swing.JLabel();
        updateitems = new javax.swing.JPanel();
        updateitemspanel1 = new javax.swing.JPanel();
        updatepanelitemnamepanel = new javax.swing.JPanel();
        updateitemnamelabel = new javax.swing.JLabel();
        updateitemnametft = new javax.swing.JTextField();
        updatepanelcompanynamepanel = new javax.swing.JPanel();
        updatecompanynamepanel = new javax.swing.JLabel();
        updatecompanynametft = new javax.swing.JTextField();
        updatepanelitempricepanel = new javax.swing.JPanel();
        updateitempricelabel = new javax.swing.JLabel();
        updateitempricetft = new javax.swing.JTextField();
        updatepanelitemsalepricepanel = new javax.swing.JPanel();
        updateitemsalepricelabel = new javax.swing.JLabel();
        updateitemsalepricetft = new javax.swing.JTextField();
        updatepanelitemquantitypanel = new javax.swing.JPanel();
        updateitemquantitylabel = new javax.swing.JLabel();
        updateitemquantitytft = new javax.swing.JTextField();
        updatepanelitemsaletypepanel = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        updatecombobox = new javax.swing.JComboBox<>();
        updatepanelupdatebtn = new javax.swing.JButton();
        updatepanelcancelbtn = new javax.swing.JButton();
        updatesearchpanel = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        updatesearchcombo = new javax.swing.JComboBox<>();
        removeitemspanel = new javax.swing.JPanel();
        updatesearchpanel1 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        removesearchcombo = new javax.swing.JComboBox<>();
        removeitemspanel2 = new javax.swing.JPanel();
        updatepanelitemnamepanel2 = new javax.swing.JPanel();
        updateitemnamelabel2 = new javax.swing.JLabel();
        removeitemnametft = new javax.swing.JTextField();
        updatepanelcompanynamepanel2 = new javax.swing.JPanel();
        updatecompanynamepanel2 = new javax.swing.JLabel();
        removecompanynametft = new javax.swing.JTextField();
        updatepanelitempricepanel2 = new javax.swing.JPanel();
        updateitempricelabel2 = new javax.swing.JLabel();
        removeitempricetft = new javax.swing.JTextField();
        updatepanelitemsalepricepanel2 = new javax.swing.JPanel();
        updateitemsalepricelabel2 = new javax.swing.JLabel();
        removeitemsalepricetft = new javax.swing.JTextField();
        updatepanelitemquantitypanel2 = new javax.swing.JPanel();
        updateitemquantitylabel2 = new javax.swing.JLabel();
        removeitemquantitytft = new javax.swing.JTextField();
        updatepanelitemsaletypepanel2 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        removesaletypecombo = new javax.swing.JComboBox<>();
        removeitembtn = new javax.swing.JButton();
        removecancelbtn = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        systemjmenu1 = new javax.swing.JMenu();
        jmenuhold = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Point OF Sale");
        setBackground(new java.awt.Color(228, 241, 254));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        parentpanel.setBackground(new java.awt.Color(228, 241, 254));
        parentpanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        parentpanel.setLayout(new java.awt.BorderLayout());

        leftpanel.setBackground(new java.awt.Color(1, 50, 67));
        leftpanel.setPreferredSize(new java.awt.Dimension(310, 824));

        jPanel1.setBackground(new java.awt.Color(1, 50, 67));

        homelabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        homelabel.setForeground(new java.awt.Color(228, 241, 254));
        homelabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        homelabel.setText("HOME");
        homelabel.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                homelabelFocusGained(evt);
            }
        });
        homelabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                homelabelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                homelabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                homelabelMouseExited(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/icon_home.png"))); // NOI18N

        additemslabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        additemslabel.setForeground(new java.awt.Color(228, 241, 254));
        additemslabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        additemslabel.setText("Add Items");
        additemslabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                additemslabelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                additemslabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                additemslabelMouseExited(evt);
            }
        });

        updateitemslabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        updateitemslabel.setForeground(new java.awt.Color(228, 241, 254));
        updateitemslabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        updateitemslabel.setText("Update");
        updateitemslabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                updateitemslabelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                updateitemslabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                updateitemslabelMouseExited(evt);
            }
        });

        removeitemslabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        removeitemslabel.setForeground(new java.awt.Color(228, 241, 254));
        removeitemslabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        removeitemslabel.setText("Remove");
        removeitemslabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                removeitemslabelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                removeitemslabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                removeitemslabelMouseExited(evt);
            }
        });

        homestarlabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        homestarlabel.setForeground(new java.awt.Color(228, 241, 254));
        homestarlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        homestarlabel.setText("*");

        addstarlabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        addstarlabel.setForeground(new java.awt.Color(228, 241, 254));
        addstarlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addstarlabel.setText("*");

        removestarlabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        removestarlabel.setForeground(new java.awt.Color(228, 241, 254));
        removestarlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        removestarlabel.setText("*");

        updatestarlabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        updatestarlabel.setForeground(new java.awt.Color(228, 241, 254));
        updatestarlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        updatestarlabel.setText("*");

        accountlabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        accountlabel.setForeground(new java.awt.Color(228, 241, 254));
        accountlabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        accountlabel.setText("Account");
        accountlabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                accountlabelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                accountlabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                accountlabelMouseExited(evt);
            }
        });

        stocklabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        stocklabel.setForeground(new java.awt.Color(228, 241, 254));
        stocklabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        stocklabel.setText("Stock");
        stocklabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stocklabelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                stocklabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                stocklabelMouseExited(evt);
            }
        });

        accountstarlabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        accountstarlabel.setForeground(new java.awt.Color(228, 241, 254));
        accountstarlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        accountstarlabel.setText("*");

        stockstarlabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        stockstarlabel.setForeground(new java.awt.Color(228, 241, 254));
        stockstarlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        stockstarlabel.setText("*");

        salegraphlabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        salegraphlabel.setForeground(new java.awt.Color(228, 241, 254));
        salegraphlabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        salegraphlabel.setText("Sale Graph");
        salegraphlabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                salegraphlabelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                salegraphlabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                salegraphlabelMouseExited(evt);
            }
        });

        salegraphstarlabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        salegraphstarlabel.setForeground(new java.awt.Color(228, 241, 254));
        salegraphstarlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        salegraphstarlabel.setText("*");

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/addicon.png"))); // NOI18N

        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/icon_update.png"))); // NOI18N

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/icon_remove.png"))); // NOI18N

        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/icon_accounts.png"))); // NOI18N

        jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/icon_stock.png"))); // NOI18N

        jLabel23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/icon_graph.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(accountlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(removeitemslabel, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(salegraphlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(stocklabel, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel1)
                                .addComponent(jLabel18, javax.swing.GroupLayout.Alignment.LEADING))
                            .addComponent(jLabel19, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(updateitemslabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(additemslabel, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                            .addComponent(homelabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(removestarlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homestarlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addstarlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(updatestarlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(accountstarlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stockstarlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(salegraphstarlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(34, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(homelabel)
                        .addComponent(homestarlabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(additemslabel)
                        .addComponent(addstarlabel))
                    .addComponent(jLabel18))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(updateitemslabel)
                        .addComponent(updatestarlabel))
                    .addComponent(jLabel19))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(removestarlabel)
                        .addComponent(removeitemslabel))
                    .addComponent(jLabel20))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(accountlabel)
                        .addComponent(accountstarlabel))
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(stockstarlabel)
                    .addComponent(stocklabel)
                    .addComponent(jLabel22))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(salegraphlabel)
                        .addComponent(salegraphstarlabel))
                    .addComponent(jLabel23))
                .addGap(21, 21, 21))
        );

        shopnamepanel.setBackground(new java.awt.Color(1, 50, 67));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(228, 241, 254));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("HIGHLIGHT");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(228, 241, 254));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Stainless Steel House");

        poslabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        poslabel.setForeground(new java.awt.Color(228, 241, 254));
        poslabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        poslabel.setText("POINT OF SALE");

        javax.swing.GroupLayout shopnamepanelLayout = new javax.swing.GroupLayout(shopnamepanel);
        shopnamepanel.setLayout(shopnamepanelLayout);
        shopnamepanelLayout.setHorizontalGroup(
            shopnamepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shopnamepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(shopnamepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, shopnamepanelLayout.createSequentialGroup()
                        .addGroup(shopnamepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(poslabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, shopnamepanelLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(13, 13, 13)))
                        .addGap(17, 17, 17))))
        );
        shopnamepanelLayout.setVerticalGroup(
            shopnamepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(shopnamepanelLayout.createSequentialGroup()
                .addGroup(shopnamepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(shopnamepanelLayout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(jLabel5)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                .addComponent(poslabel, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout leftpanelLayout = new javax.swing.GroupLayout(leftpanel);
        leftpanel.setLayout(leftpanelLayout);
        leftpanelLayout.setHorizontalGroup(
            leftpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(leftpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(shopnamepanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(leftpanelLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        leftpanelLayout.setVerticalGroup(
            leftpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(leftpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(shopnamepanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(364, Short.MAX_VALUE))
        );

        parentpanel.add(leftpanel, java.awt.BorderLayout.LINE_START);

        rightpanel.setBackground(new java.awt.Color(228, 241, 254));
        rightpanel.setPreferredSize(new java.awt.Dimension(300, 577));

        billingpanel.setBackground(new java.awt.Color(1, 50, 67));
        billingpanel.setPreferredSize(new java.awt.Dimension(255, 824));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(228, 241, 254));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Billing");

        proprietername.setForeground(new java.awt.Color(228, 241, 254));
        proprietername.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        proprietername.setText("Proprieter   M Abdullah");

        billingtoppanel.setBackground(new java.awt.Color(1, 50, 67));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(228, 241, 254));
        jLabel7.setText("Total Items");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(228, 241, 254));
        jLabel8.setText("Total Amount");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(228, 241, 254));
        jLabel9.setText("Discount Allowed");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(228, 241, 254));
        jLabel10.setText("Bill");

        javax.swing.GroupLayout billingtoppanelLayout = new javax.swing.GroupLayout(billingtoppanel);
        billingtoppanel.setLayout(billingtoppanelLayout);
        billingtoppanelLayout.setHorizontalGroup(
            billingtoppanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(billingtoppanelLayout.createSequentialGroup()
                .addGroup(billingtoppanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addGroup(billingtoppanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(billingtoppanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(billtft, javax.swing.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE)
                    .addComponent(totalitemstft)
                    .addComponent(totalamounttft)
                    .addComponent(discountallowedtft)))
        );
        billingtoppanelLayout.setVerticalGroup(
            billingtoppanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(billingtoppanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(billingtoppanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(totalitemstft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(billingtoppanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(totalamounttft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(billingtoppanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(discountallowedtft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(billingtoppanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(billtft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        savebtn.setText("save");

        cancelbtn.setText("cancel");

        javax.swing.GroupLayout billingpanelLayout = new javax.swing.GroupLayout(billingpanel);
        billingpanel.setLayout(billingpanelLayout);
        billingpanelLayout.setHorizontalGroup(
            billingpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(billingpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(billingpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, billingpanelLayout.createSequentialGroup()
                        .addGap(0, 122, Short.MAX_VALUE)
                        .addGroup(billingpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, billingpanelLayout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(50, 50, 50))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, billingpanelLayout.createSequentialGroup()
                                .addComponent(savebtn, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))
                    .addGroup(billingpanelLayout.createSequentialGroup()
                        .addGroup(billingpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(proprietername, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(billingtoppanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(billingpanelLayout.createSequentialGroup()
                                .addComponent(cancelbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        billingpanelLayout.setVerticalGroup(
            billingpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(billingpanelLayout.createSequentialGroup()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(proprietername)
                .addGap(18, 18, 18)
                .addComponent(billingtoppanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(billingpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(savebtn)
                    .addComponent(cancelbtn))
                .addContainerGap(169, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout rightpanelLayout = new javax.swing.GroupLayout(rightpanel);
        rightpanel.setLayout(rightpanelLayout);
        rightpanelLayout.setHorizontalGroup(
            rightpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rightpanelLayout.createSequentialGroup()
                .addComponent(billingpanel, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 18, Short.MAX_VALUE))
        );
        rightpanelLayout.setVerticalGroup(
            rightpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, rightpanelLayout.createSequentialGroup()
                .addContainerGap(316, Short.MAX_VALUE)
                .addComponent(billingpanel, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(114, 114, 114))
        );

        parentpanel.add(rightpanel, java.awt.BorderLayout.LINE_END);

        centerpanel.setBackground(new java.awt.Color(102, 51, 0));
        centerpanel.setLayout(new java.awt.CardLayout());

        salegraphpanel.setBackground(new java.awt.Color(228, 241, 254));
        salegraphpanel.setAutoscrolls(true);

        salegraphheaderpanel.setBackground(new java.awt.Color(1, 50, 67));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(228, 241, 254));
        jLabel15.setText("Sales Graph");

        javax.swing.GroupLayout salegraphheaderpanelLayout = new javax.swing.GroupLayout(salegraphheaderpanel);
        salegraphheaderpanel.setLayout(salegraphheaderpanelLayout);
        salegraphheaderpanelLayout.setHorizontalGroup(
            salegraphheaderpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(salegraphheaderpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15)
                .addContainerGap(18, Short.MAX_VALUE))
        );
        salegraphheaderpanelLayout.setVerticalGroup(
            salegraphheaderpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(salegraphheaderpanelLayout.createSequentialGroup()
                .addComponent(jLabel15)
                .addGap(0, 2, Short.MAX_VALUE))
        );

        graphheaderpanel.setBackground(new java.awt.Color(1, 50, 67));

        monthradio.setText("Graph of Month");

        yearradio.setText("Graph of Year");

        datechooserofmonth.setDateFormatString("yyyy,MM");

        datechooseryear.setDateFormatString("yyyy");

        printgraphbtn.setText("Go");

        javax.swing.GroupLayout graphheaderpanelLayout = new javax.swing.GroupLayout(graphheaderpanel);
        graphheaderpanel.setLayout(graphheaderpanelLayout);
        graphheaderpanelLayout.setHorizontalGroup(
            graphheaderpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(graphheaderpanelLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(graphheaderpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(monthradio, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE)
                    .addComponent(yearradio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(53, 53, 53)
                .addGroup(graphheaderpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(datechooserofmonth, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(datechooseryear, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(68, 68, 68)
                .addComponent(printgraphbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(59, Short.MAX_VALUE))
        );
        graphheaderpanelLayout.setVerticalGroup(
            graphheaderpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(graphheaderpanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(graphheaderpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(printgraphbtn)
                    .addGroup(graphheaderpanelLayout.createSequentialGroup()
                        .addGroup(graphheaderpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(datechooserofmonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(monthradio))
                        .addGap(38, 38, 38)
                        .addGroup(graphheaderpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(yearradio, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(datechooseryear, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );

        javax.swing.GroupLayout salegraphpanelLayout = new javax.swing.GroupLayout(salegraphpanel);
        salegraphpanel.setLayout(salegraphpanelLayout);
        salegraphpanelLayout.setHorizontalGroup(
            salegraphpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(salegraphpanelLayout.createSequentialGroup()
                .addGroup(salegraphpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(salegraphpanelLayout.createSequentialGroup()
                        .addGap(303, 303, 303)
                        .addComponent(salegraphheaderpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(salegraphpanelLayout.createSequentialGroup()
                        .addGap(88, 88, 88)
                        .addComponent(graphheaderpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(283, Short.MAX_VALUE))
        );
        salegraphpanelLayout.setVerticalGroup(
            salegraphpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(salegraphpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(salegraphheaderpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(graphheaderpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(658, Short.MAX_VALUE))
        );

        centerpanel.add(salegraphpanel, "card8");

        stockpanel.setBackground(new java.awt.Color(228, 241, 254));

        stocktable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Item Name", "Company Name", "Sale Price", "Sale Type", "Remaining Quantity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(stocktable);

        stockheaderpanel.setBackground(new java.awt.Color(1, 50, 67));

        jLabel16.setBackground(new java.awt.Color(228, 241, 254));
        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(228, 241, 254));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("HIGHLIGHT SALES");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(228, 241, 254));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setText("Total Items in Store");

        showstockbtn.setText("Show Stock");

        javax.swing.GroupLayout stockheaderpanelLayout = new javax.swing.GroupLayout(stockheaderpanel);
        stockheaderpanel.setLayout(stockheaderpanelLayout);
        stockheaderpanelLayout.setHorizontalGroup(
            stockheaderpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stockheaderpanelLayout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(stockheaderpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(totalstocktft, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addComponent(showstockbtn)
                .addGap(25, 25, 25))
        );
        stockheaderpanelLayout.setVerticalGroup(
            stockheaderpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stockheaderpanelLayout.createSequentialGroup()
                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(stockheaderpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totalstocktft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(showstockbtn))
                .addGap(0, 1, Short.MAX_VALUE))
        );

        cleartotalstocktable.setText("Clear");

        jPanel5.setBackground(new java.awt.Color(228, 241, 254));

        jPanel4.setBackground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 13, Short.MAX_VALUE)
        );

        jLabel24.setText("Stock Finished");

        jPanel2.setBackground(new java.awt.Color(255, 255, 0));
        jPanel2.setPreferredSize(new java.awt.Dimension(11, 13));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 13, Short.MAX_VALUE)
        );

        jLabel25.setText("Warning");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel24)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel25)
                .addGap(0, 120, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel25)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout stockpanelLayout = new javax.swing.GroupLayout(stockpanel);
        stockpanel.setLayout(stockpanelLayout);
        stockpanelLayout.setHorizontalGroup(
            stockpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stockpanelLayout.createSequentialGroup()
                .addGroup(stockpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(stockpanelLayout.createSequentialGroup()
                        .addGap(82, 82, 82)
                        .addGroup(stockpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(stockpanelLayout.createSequentialGroup()
                                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cleartotalstocktable))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 637, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(stockpanelLayout.createSequentialGroup()
                        .addGap(181, 181, 181)
                        .addComponent(stockheaderpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(194, Short.MAX_VALUE))
        );
        stockpanelLayout.setVerticalGroup(
            stockpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stockpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(stockheaderpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 516, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(stockpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cleartotalstocktable)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(192, Short.MAX_VALUE))
        );

        centerpanel.add(stockpanel, "card7");

        accountpanel.setBackground(new java.awt.Color(228, 241, 254));

        accountsearchpanel.setBackground(new java.awt.Color(1, 50, 67));

        accountsearchcombo.setEditable(true);
        accountsearchcombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accountsearchcomboActionPerformed(evt);
            }
        });
        accountsearchcombo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                accountsearchcomboKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout accountsearchpanelLayout = new javax.swing.GroupLayout(accountsearchpanel);
        accountsearchpanel.setLayout(accountsearchpanelLayout);
        accountsearchpanelLayout.setHorizontalGroup(
            accountsearchpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(accountsearchpanelLayout.createSequentialGroup()
                .addContainerGap(27, Short.MAX_VALUE)
                .addComponent(accountsearchcombo, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel26)
                .addGap(26, 26, 26))
        );
        accountsearchpanelLayout.setVerticalGroup(
            accountsearchpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, accountsearchpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(accountsearchpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(accountsearchpanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(accountsearchcombo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Holder Name", "Cradit amount", "Last Update", "Description"
            }
        ));
        jScrollPane3.setViewportView(jTable1);

        javax.swing.GroupLayout accountpanelLayout = new javax.swing.GroupLayout(accountpanel);
        accountpanel.setLayout(accountpanelLayout);
        accountpanelLayout.setHorizontalGroup(
            accountpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(accountpanelLayout.createSequentialGroup()
                .addGroup(accountpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(accountpanelLayout.createSequentialGroup()
                        .addGap(279, 279, 279)
                        .addComponent(accountsearchpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(accountpanelLayout.createSequentialGroup()
                        .addGap(178, 178, 178)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(283, Short.MAX_VALUE))
        );
        accountpanelLayout.setVerticalGroup(
            accountpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(accountpanelLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(accountsearchpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(279, Short.MAX_VALUE))
        );

        centerpanel.add(accountpanel, "card6");

        homepanel.setBackground(new java.awt.Color(228, 241, 254));

        jPanel3.setBackground(new java.awt.Color(1, 50, 67));

        combosearch.setEditable(true);
        combosearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                combosearchActionPerformed(evt);
            }
        });
        combosearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                combosearchKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(27, Short.MAX_VALUE)
                .addComponent(combosearch, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addGap(26, 26, 26))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(combosearch, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        homesearchtable.setBackground(new java.awt.Color(32, 136, 203));
        homesearchtable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Item ID", "Item name", "Company name", "Sale price", "Sale type", "Quantity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        homesearchtable.setGridColor(new java.awt.Color(1, 50, 67));
        jScrollPane1.setViewportView(homesearchtable);

        javax.swing.GroupLayout homepanelLayout = new javax.swing.GroupLayout(homepanel);
        homepanel.setLayout(homepanelLayout);
        homepanelLayout.setHorizontalGroup(
            homepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(homepanelLayout.createSequentialGroup()
                .addContainerGap(247, Short.MAX_VALUE)
                .addGroup(homepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, homepanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 640, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, homepanelLayout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(166, 166, 166))))
        );
        homepanelLayout.setVerticalGroup(
            homepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(homepanelLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(416, Short.MAX_VALUE))
        );

        centerpanel.add(homepanel, "card5");

        additems.setBackground(new java.awt.Color(228, 241, 254));

        additemspanel2.setBackground(new java.awt.Color(1, 50, 67));

        updatepanelitemnamepanel1.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitemnamepanel1.setPreferredSize(new java.awt.Dimension(227, 70));

        updateitemnamelabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updateitemnamelabel1.setForeground(new java.awt.Color(228, 241, 254));
        updateitemnamelabel1.setText("Item Name");

        itemnametft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        itemnametft.setMinimumSize(new java.awt.Dimension(6, 23));

        javax.swing.GroupLayout updatepanelitemnamepanel1Layout = new javax.swing.GroupLayout(updatepanelitemnamepanel1);
        updatepanelitemnamepanel1.setLayout(updatepanelitemnamepanel1Layout);
        updatepanelitemnamepanel1Layout.setHorizontalGroup(
            updatepanelitemnamepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemnamepanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updateitemnamelabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addComponent(itemnametft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelitemnamepanel1Layout.setVerticalGroup(
            updatepanelitemnamepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemnamepanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitemnamepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateitemnamelabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(itemnametft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        updatepanelcompanynamepanel1.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelcompanynamepanel1.setPreferredSize(new java.awt.Dimension(301, 41));

        updatecompanynamepanel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updatecompanynamepanel1.setForeground(new java.awt.Color(228, 241, 254));
        updatecompanynamepanel1.setText("Company Name");

        companynametft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        companynametft.setMinimumSize(new java.awt.Dimension(6, 23));
        companynametft.setPreferredSize(new java.awt.Dimension(9, 20));

        javax.swing.GroupLayout updatepanelcompanynamepanel1Layout = new javax.swing.GroupLayout(updatepanelcompanynamepanel1);
        updatepanelcompanynamepanel1.setLayout(updatepanelcompanynamepanel1Layout);
        updatepanelcompanynamepanel1Layout.setHorizontalGroup(
            updatepanelcompanynamepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelcompanynamepanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updatecompanynamepanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addComponent(companynametft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelcompanynamepanel1Layout.setVerticalGroup(
            updatepanelcompanynamepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelcompanynamepanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelcompanynamepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updatecompanynamepanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(companynametft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        updatepanelitempricepanel1.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitempricepanel1.setPreferredSize(new java.awt.Dimension(301, 41));

        updateitempricelabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updateitempricelabel1.setForeground(new java.awt.Color(228, 241, 254));
        updateitempricelabel1.setText("Item Price");

        itempricetft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        itempricetft.setPreferredSize(new java.awt.Dimension(15, 23));

        javax.swing.GroupLayout updatepanelitempricepanel1Layout = new javax.swing.GroupLayout(updatepanelitempricepanel1);
        updatepanelitempricepanel1.setLayout(updatepanelitempricepanel1Layout);
        updatepanelitempricepanel1Layout.setHorizontalGroup(
            updatepanelitempricepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitempricepanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updateitempricelabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(itempricetft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelitempricepanel1Layout.setVerticalGroup(
            updatepanelitempricepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitempricepanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitempricepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateitempricelabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(itempricetft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27))
        );

        updatepanelitemsalepricepanel1.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitemsalepricepanel1.setPreferredSize(new java.awt.Dimension(301, 41));

        updateitemsalepricelabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updateitemsalepricelabel1.setForeground(new java.awt.Color(228, 241, 254));
        updateitemsalepricelabel1.setText("Item Sale Price");

        itemsalepricetft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        itemsalepricetft.setPreferredSize(new java.awt.Dimension(6, 23));

        javax.swing.GroupLayout updatepanelitemsalepricepanel1Layout = new javax.swing.GroupLayout(updatepanelitemsalepricepanel1);
        updatepanelitemsalepricepanel1.setLayout(updatepanelitemsalepricepanel1Layout);
        updatepanelitemsalepricepanel1Layout.setHorizontalGroup(
            updatepanelitemsalepricepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemsalepricepanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updateitemsalepricelabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(itemsalepricetft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelitemsalepricepanel1Layout.setVerticalGroup(
            updatepanelitemsalepricepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemsalepricepanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitemsalepricepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateitemsalepricelabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(itemsalepricetft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
        );

        updatepanelitemquantitypanel1.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitemquantitypanel1.setPreferredSize(new java.awt.Dimension(301, 41));

        updateitemquantitylabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updateitemquantitylabel1.setForeground(new java.awt.Color(228, 241, 254));
        updateitemquantitylabel1.setText("Item Quantity");

        itemquantitytft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        itemquantitytft.setPreferredSize(new java.awt.Dimension(6, 23));

        javax.swing.GroupLayout updatepanelitemquantitypanel1Layout = new javax.swing.GroupLayout(updatepanelitemquantitypanel1);
        updatepanelitemquantitypanel1.setLayout(updatepanelitemquantitypanel1Layout);
        updatepanelitemquantitypanel1Layout.setHorizontalGroup(
            updatepanelitemquantitypanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemquantitypanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updateitemquantitylabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(itemquantitytft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelitemquantitypanel1Layout.setVerticalGroup(
            updatepanelitemquantitypanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemquantitypanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitemquantitypanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateitemquantitylabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(itemquantitytft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        updatepanelitemsaletypepanel1.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitemsaletypepanel1.setPreferredSize(new java.awt.Dimension(301, 41));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(228, 241, 254));
        jLabel12.setText("Item Sale Type");

        itemsaletypecombo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dozen", "Half Dozen", "Pieces", "Set", "KG" }));

        javax.swing.GroupLayout updatepanelitemsaletypepanel1Layout = new javax.swing.GroupLayout(updatepanelitemsaletypepanel1);
        updatepanelitemsaletypepanel1.setLayout(updatepanelitemsaletypepanel1Layout);
        updatepanelitemsaletypepanel1Layout.setHorizontalGroup(
            updatepanelitemsaletypepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemsaletypepanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(itemsaletypecombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        updatepanelitemsaletypepanel1Layout.setVerticalGroup(
            updatepanelitemsaletypepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemsaletypepanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitemsaletypepanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(itemsaletypecombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        saveadditembtn.setBackground(new java.awt.Color(1, 50, 67));
        saveadditembtn.setForeground(new java.awt.Color(228, 241, 254));
        saveadditembtn.setText("Save");
        saveadditembtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveadditembtnActionPerformed(evt);
            }
        });

        canceladditembtn.setBackground(new java.awt.Color(1, 50, 67));
        canceladditembtn.setForeground(new java.awt.Color(228, 241, 254));
        canceladditembtn.setText("Cancel");
        canceladditembtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                canceladditembtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout additemspanel2Layout = new javax.swing.GroupLayout(additemspanel2);
        additemspanel2.setLayout(additemspanel2Layout);
        additemspanel2Layout.setHorizontalGroup(
            additemspanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(additemspanel2Layout.createSequentialGroup()
                .addGap(119, 119, 119)
                .addGroup(additemspanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(additemspanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(updatepanelcompanynamepanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(additemspanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(updatepanelitemquantitypanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(updatepanelitemsalepricepanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(updatepanelitemnamepanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
                            .addComponent(updatepanelitempricepanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(updatepanelitemsaletypepanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(additemspanel2Layout.createSequentialGroup()
                        .addComponent(canceladditembtn)
                        .addGap(44, 44, 44)
                        .addComponent(saveadditembtn)))
                .addContainerGap(123, Short.MAX_VALUE))
        );
        additemspanel2Layout.setVerticalGroup(
            additemspanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(additemspanel2Layout.createSequentialGroup()
                .addComponent(updatepanelitemnamepanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelcompanynamepanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelitempricepanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelitemsalepricepanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelitemquantitypanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelitemsaletypepanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(additemspanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveadditembtn)
                    .addComponent(canceladditembtn))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        additemspanel.setBackground(new java.awt.Color(1, 50, 67));

        additemspanellabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        additemspanellabel.setForeground(new java.awt.Color(228, 241, 254));
        additemspanellabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        additemspanellabel.setText("HIGHLIGHT STORE");

        javax.swing.GroupLayout additemspanelLayout = new javax.swing.GroupLayout(additemspanel);
        additemspanel.setLayout(additemspanelLayout);
        additemspanelLayout.setHorizontalGroup(
            additemspanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, additemspanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(additemspanellabel, javax.swing.GroupLayout.PREFERRED_SIZE, 376, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        additemspanelLayout.setVerticalGroup(
            additemspanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, additemspanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(additemspanellabel, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout additemsLayout = new javax.swing.GroupLayout(additems);
        additems.setLayout(additemsLayout);
        additemsLayout.setHorizontalGroup(
            additemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(additemsLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(additemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, additemsLayout.createSequentialGroup()
                        .addComponent(additemspanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(127, 127, 127))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, additemsLayout.createSequentialGroup()
                        .addComponent(additemspanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42))))
        );
        additemsLayout.setVerticalGroup(
            additemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, additemsLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(additemspanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(additemspanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(228, Short.MAX_VALUE))
        );

        centerpanel.add(additems, "card4");

        updateitems.setBackground(new java.awt.Color(228, 241, 254));

        updateitemspanel1.setBackground(new java.awt.Color(1, 50, 67));

        updatepanelitemnamepanel.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitemnamepanel.setPreferredSize(new java.awt.Dimension(227, 70));

        updateitemnamelabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updateitemnamelabel.setForeground(new java.awt.Color(228, 241, 254));
        updateitemnamelabel.setText("Item Name");

        updateitemnametft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        updateitemnametft.setMinimumSize(new java.awt.Dimension(6, 23));

        javax.swing.GroupLayout updatepanelitemnamepanelLayout = new javax.swing.GroupLayout(updatepanelitemnamepanel);
        updatepanelitemnamepanel.setLayout(updatepanelitemnamepanelLayout);
        updatepanelitemnamepanelLayout.setHorizontalGroup(
            updatepanelitemnamepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemnamepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updateitemnamelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addComponent(updateitemnametft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelitemnamepanelLayout.setVerticalGroup(
            updatepanelitemnamepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemnamepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitemnamepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateitemnamelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(updateitemnametft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        updatepanelcompanynamepanel.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelcompanynamepanel.setPreferredSize(new java.awt.Dimension(301, 41));

        updatecompanynamepanel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updatecompanynamepanel.setForeground(new java.awt.Color(228, 241, 254));
        updatecompanynamepanel.setText("Company Name");

        updatecompanynametft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        updatecompanynametft.setMinimumSize(new java.awt.Dimension(6, 23));
        updatecompanynametft.setPreferredSize(new java.awt.Dimension(9, 20));

        javax.swing.GroupLayout updatepanelcompanynamepanelLayout = new javax.swing.GroupLayout(updatepanelcompanynamepanel);
        updatepanelcompanynamepanel.setLayout(updatepanelcompanynamepanelLayout);
        updatepanelcompanynamepanelLayout.setHorizontalGroup(
            updatepanelcompanynamepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelcompanynamepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updatecompanynamepanel, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addComponent(updatecompanynametft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelcompanynamepanelLayout.setVerticalGroup(
            updatepanelcompanynamepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelcompanynamepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelcompanynamepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updatecompanynamepanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(updatecompanynametft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        updatepanelitempricepanel.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitempricepanel.setPreferredSize(new java.awt.Dimension(301, 41));

        updateitempricelabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updateitempricelabel.setForeground(new java.awt.Color(228, 241, 254));
        updateitempricelabel.setText("Item Price");

        updateitempricetft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        updateitempricetft.setPreferredSize(new java.awt.Dimension(15, 23));

        javax.swing.GroupLayout updatepanelitempricepanelLayout = new javax.swing.GroupLayout(updatepanelitempricepanel);
        updatepanelitempricepanel.setLayout(updatepanelitempricepanelLayout);
        updatepanelitempricepanelLayout.setHorizontalGroup(
            updatepanelitempricepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitempricepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updateitempricelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(updateitempricetft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelitempricepanelLayout.setVerticalGroup(
            updatepanelitempricepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitempricepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitempricepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateitempricelabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(updateitempricetft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27))
        );

        updatepanelitemsalepricepanel.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitemsalepricepanel.setPreferredSize(new java.awt.Dimension(301, 41));

        updateitemsalepricelabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updateitemsalepricelabel.setForeground(new java.awt.Color(228, 241, 254));
        updateitemsalepricelabel.setText("Item Sale Price");

        updateitemsalepricetft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        updateitemsalepricetft.setPreferredSize(new java.awt.Dimension(6, 23));

        javax.swing.GroupLayout updatepanelitemsalepricepanelLayout = new javax.swing.GroupLayout(updatepanelitemsalepricepanel);
        updatepanelitemsalepricepanel.setLayout(updatepanelitemsalepricepanelLayout);
        updatepanelitemsalepricepanelLayout.setHorizontalGroup(
            updatepanelitemsalepricepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemsalepricepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updateitemsalepricelabel, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(updateitemsalepricetft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelitemsalepricepanelLayout.setVerticalGroup(
            updatepanelitemsalepricepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemsalepricepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitemsalepricepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateitemsalepricelabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(updateitemsalepricetft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
        );

        updatepanelitemquantitypanel.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitemquantitypanel.setPreferredSize(new java.awt.Dimension(301, 41));

        updateitemquantitylabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updateitemquantitylabel.setForeground(new java.awt.Color(228, 241, 254));
        updateitemquantitylabel.setText("Item Quantity");

        updateitemquantitytft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        updateitemquantitytft.setPreferredSize(new java.awt.Dimension(6, 23));

        javax.swing.GroupLayout updatepanelitemquantitypanelLayout = new javax.swing.GroupLayout(updatepanelitemquantitypanel);
        updatepanelitemquantitypanel.setLayout(updatepanelitemquantitypanelLayout);
        updatepanelitemquantitypanelLayout.setHorizontalGroup(
            updatepanelitemquantitypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemquantitypanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updateitemquantitylabel, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(updateitemquantitytft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelitemquantitypanelLayout.setVerticalGroup(
            updatepanelitemquantitypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemquantitypanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitemquantitypanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateitemquantitylabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(updateitemquantitytft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        updatepanelitemsaletypepanel.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitemsaletypepanel.setPreferredSize(new java.awt.Dimension(301, 41));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(228, 241, 254));
        jLabel6.setText("Item Sale Type");

        updatecombobox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dozen", "Half Dozen", "Pieces", "Set", "Kg" }));

        javax.swing.GroupLayout updatepanelitemsaletypepanelLayout = new javax.swing.GroupLayout(updatepanelitemsaletypepanel);
        updatepanelitemsaletypepanel.setLayout(updatepanelitemsaletypepanelLayout);
        updatepanelitemsaletypepanelLayout.setHorizontalGroup(
            updatepanelitemsaletypepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemsaletypepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(updatecombobox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        updatepanelitemsaletypepanelLayout.setVerticalGroup(
            updatepanelitemsaletypepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemsaletypepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitemsaletypepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(updatecombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        updatepanelupdatebtn.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelupdatebtn.setForeground(new java.awt.Color(228, 241, 254));
        updatepanelupdatebtn.setText("Update");
        updatepanelupdatebtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updatepanelupdatebtnActionPerformed(evt);
            }
        });

        updatepanelcancelbtn.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelcancelbtn.setForeground(new java.awt.Color(228, 241, 254));
        updatepanelcancelbtn.setText("Cancel");
        updatepanelcancelbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updatepanelcancelbtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout updateitemspanel1Layout = new javax.swing.GroupLayout(updateitemspanel1);
        updateitemspanel1.setLayout(updateitemspanel1Layout);
        updateitemspanel1Layout.setHorizontalGroup(
            updateitemspanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updateitemspanel1Layout.createSequentialGroup()
                .addGap(119, 119, 119)
                .addGroup(updateitemspanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(updateitemspanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(updatepanelcompanynamepanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(updateitemspanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(updatepanelitemquantitypanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(updatepanelitemsalepricepanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(updatepanelitemnamepanel, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
                            .addComponent(updatepanelitempricepanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(updatepanelitemsaletypepanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(updateitemspanel1Layout.createSequentialGroup()
                        .addComponent(updatepanelcancelbtn)
                        .addGap(44, 44, 44)
                        .addComponent(updatepanelupdatebtn)))
                .addContainerGap(123, Short.MAX_VALUE))
        );
        updateitemspanel1Layout.setVerticalGroup(
            updateitemspanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updateitemspanel1Layout.createSequentialGroup()
                .addComponent(updatepanelitemnamepanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelcompanynamepanel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelitempricepanel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelitemsalepricepanel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelitemquantitypanel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelitemsaletypepanel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(updateitemspanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updatepanelupdatebtn)
                    .addComponent(updatepanelcancelbtn))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        updatesearchpanel.setBackground(new java.awt.Color(1, 50, 67));

        updatesearchcombo.setEditable(true);
        updatesearchcombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updatesearchcomboActionPerformed(evt);
            }
        });
        updatesearchcombo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                updatesearchcomboKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout updatesearchpanelLayout = new javax.swing.GroupLayout(updatesearchpanel);
        updatesearchpanel.setLayout(updatesearchpanelLayout);
        updatesearchpanelLayout.setHorizontalGroup(
            updatesearchpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatesearchpanelLayout.createSequentialGroup()
                .addContainerGap(27, Short.MAX_VALUE)
                .addComponent(updatesearchcombo, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11)
                .addGap(26, 26, 26))
        );
        updatesearchpanelLayout.setVerticalGroup(
            updatesearchpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, updatesearchpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatesearchpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(updatesearchpanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(updatesearchcombo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout updateitemsLayout = new javax.swing.GroupLayout(updateitems);
        updateitems.setLayout(updateitemsLayout);
        updateitemsLayout.setHorizontalGroup(
            updateitemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updateitemsLayout.createSequentialGroup()
                .addContainerGap(360, Short.MAX_VALUE)
                .addGroup(updateitemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, updateitemsLayout.createSequentialGroup()
                        .addComponent(updateitemspanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, updateitemsLayout.createSequentialGroup()
                        .addComponent(updatesearchpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(167, 167, 167))))
        );
        updateitemsLayout.setVerticalGroup(
            updateitemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updateitemsLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(updatesearchpanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updateitemspanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(258, Short.MAX_VALUE))
        );

        centerpanel.add(updateitems, "card3");

        removeitemspanel.setBackground(new java.awt.Color(228, 241, 254));

        updatesearchpanel1.setBackground(new java.awt.Color(1, 50, 67));

        removesearchcombo.setEditable(true);
        removesearchcombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removesearchcomboActionPerformed(evt);
            }
        });
        removesearchcombo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                removesearchcomboKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout updatesearchpanel1Layout = new javax.swing.GroupLayout(updatesearchpanel1);
        updatesearchpanel1.setLayout(updatesearchpanel1Layout);
        updatesearchpanel1Layout.setHorizontalGroup(
            updatesearchpanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatesearchpanel1Layout.createSequentialGroup()
                .addContainerGap(27, Short.MAX_VALUE)
                .addComponent(removesearchcombo, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13)
                .addGap(26, 26, 26))
        );
        updatesearchpanel1Layout.setVerticalGroup(
            updatesearchpanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, updatesearchpanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatesearchpanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(updatesearchpanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(removesearchcombo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        removeitemspanel2.setBackground(new java.awt.Color(1, 50, 67));

        updatepanelitemnamepanel2.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitemnamepanel2.setPreferredSize(new java.awt.Dimension(227, 70));

        updateitemnamelabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updateitemnamelabel2.setForeground(new java.awt.Color(228, 241, 254));
        updateitemnamelabel2.setText("Item Name");

        removeitemnametft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        removeitemnametft.setMinimumSize(new java.awt.Dimension(6, 23));

        javax.swing.GroupLayout updatepanelitemnamepanel2Layout = new javax.swing.GroupLayout(updatepanelitemnamepanel2);
        updatepanelitemnamepanel2.setLayout(updatepanelitemnamepanel2Layout);
        updatepanelitemnamepanel2Layout.setHorizontalGroup(
            updatepanelitemnamepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemnamepanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updateitemnamelabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addComponent(removeitemnametft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelitemnamepanel2Layout.setVerticalGroup(
            updatepanelitemnamepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemnamepanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitemnamepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateitemnamelabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(removeitemnametft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        updatepanelcompanynamepanel2.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelcompanynamepanel2.setPreferredSize(new java.awt.Dimension(301, 41));

        updatecompanynamepanel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updatecompanynamepanel2.setForeground(new java.awt.Color(228, 241, 254));
        updatecompanynamepanel2.setText("Company Name");

        removecompanynametft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        removecompanynametft.setMinimumSize(new java.awt.Dimension(6, 23));
        removecompanynametft.setPreferredSize(new java.awt.Dimension(9, 20));

        javax.swing.GroupLayout updatepanelcompanynamepanel2Layout = new javax.swing.GroupLayout(updatepanelcompanynamepanel2);
        updatepanelcompanynamepanel2.setLayout(updatepanelcompanynamepanel2Layout);
        updatepanelcompanynamepanel2Layout.setHorizontalGroup(
            updatepanelcompanynamepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelcompanynamepanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updatecompanynamepanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addComponent(removecompanynametft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelcompanynamepanel2Layout.setVerticalGroup(
            updatepanelcompanynamepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelcompanynamepanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelcompanynamepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updatecompanynamepanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(removecompanynametft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        updatepanelitempricepanel2.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitempricepanel2.setPreferredSize(new java.awt.Dimension(301, 41));

        updateitempricelabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updateitempricelabel2.setForeground(new java.awt.Color(228, 241, 254));
        updateitempricelabel2.setText("Item Price");

        removeitempricetft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        removeitempricetft.setPreferredSize(new java.awt.Dimension(15, 23));

        javax.swing.GroupLayout updatepanelitempricepanel2Layout = new javax.swing.GroupLayout(updatepanelitempricepanel2);
        updatepanelitempricepanel2.setLayout(updatepanelitempricepanel2Layout);
        updatepanelitempricepanel2Layout.setHorizontalGroup(
            updatepanelitempricepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitempricepanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updateitempricelabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(removeitempricetft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelitempricepanel2Layout.setVerticalGroup(
            updatepanelitempricepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitempricepanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitempricepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateitempricelabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(removeitempricetft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27))
        );

        updatepanelitemsalepricepanel2.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitemsalepricepanel2.setPreferredSize(new java.awt.Dimension(301, 41));

        updateitemsalepricelabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updateitemsalepricelabel2.setForeground(new java.awt.Color(228, 241, 254));
        updateitemsalepricelabel2.setText("Item Sale Price");

        removeitemsalepricetft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        removeitemsalepricetft.setPreferredSize(new java.awt.Dimension(6, 23));

        javax.swing.GroupLayout updatepanelitemsalepricepanel2Layout = new javax.swing.GroupLayout(updatepanelitemsalepricepanel2);
        updatepanelitemsalepricepanel2.setLayout(updatepanelitemsalepricepanel2Layout);
        updatepanelitemsalepricepanel2Layout.setHorizontalGroup(
            updatepanelitemsalepricepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemsalepricepanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updateitemsalepricelabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(removeitemsalepricetft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelitemsalepricepanel2Layout.setVerticalGroup(
            updatepanelitemsalepricepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemsalepricepanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitemsalepricepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateitemsalepricelabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(removeitemsalepricetft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
        );

        updatepanelitemquantitypanel2.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitemquantitypanel2.setPreferredSize(new java.awt.Dimension(301, 41));

        updateitemquantitylabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        updateitemquantitylabel2.setForeground(new java.awt.Color(228, 241, 254));
        updateitemquantitylabel2.setText("Item Quantity");

        removeitemquantitytft.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        removeitemquantitytft.setPreferredSize(new java.awt.Dimension(6, 23));

        javax.swing.GroupLayout updatepanelitemquantitypanel2Layout = new javax.swing.GroupLayout(updatepanelitemquantitypanel2);
        updatepanelitemquantitypanel2.setLayout(updatepanelitemquantitypanel2Layout);
        updatepanelitemquantitypanel2Layout.setHorizontalGroup(
            updatepanelitemquantitypanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemquantitypanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(updateitemquantitylabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(removeitemquantitytft, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        updatepanelitemquantitypanel2Layout.setVerticalGroup(
            updatepanelitemquantitypanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemquantitypanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitemquantitypanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateitemquantitylabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(removeitemquantitytft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        updatepanelitemsaletypepanel2.setBackground(new java.awt.Color(1, 50, 67));
        updatepanelitemsaletypepanel2.setPreferredSize(new java.awt.Dimension(301, 41));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(228, 241, 254));
        jLabel14.setText("Item Sale Type");

        removesaletypecombo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dozen", "Pieces", "set", "KG" }));

        javax.swing.GroupLayout updatepanelitemsaletypepanel2Layout = new javax.swing.GroupLayout(updatepanelitemsaletypepanel2);
        updatepanelitemsaletypepanel2.setLayout(updatepanelitemsaletypepanel2Layout);
        updatepanelitemsaletypepanel2Layout.setHorizontalGroup(
            updatepanelitemsaletypepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemsaletypepanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(removesaletypecombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        updatepanelitemsaletypepanel2Layout.setVerticalGroup(
            updatepanelitemsaletypepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(updatepanelitemsaletypepanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(updatepanelitemsaletypepanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(removesaletypecombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        removeitembtn.setBackground(new java.awt.Color(1, 50, 67));
        removeitembtn.setForeground(new java.awt.Color(228, 241, 254));
        removeitembtn.setText("Delete");
        removeitembtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeitembtnActionPerformed(evt);
            }
        });

        removecancelbtn.setBackground(new java.awt.Color(1, 50, 67));
        removecancelbtn.setForeground(new java.awt.Color(228, 241, 254));
        removecancelbtn.setText("Cancel");
        removecancelbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removecancelbtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout removeitemspanel2Layout = new javax.swing.GroupLayout(removeitemspanel2);
        removeitemspanel2.setLayout(removeitemspanel2Layout);
        removeitemspanel2Layout.setHorizontalGroup(
            removeitemspanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(removeitemspanel2Layout.createSequentialGroup()
                .addGap(119, 119, 119)
                .addGroup(removeitemspanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(removeitemspanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(updatepanelcompanynamepanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(removeitemspanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(updatepanelitemquantitypanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(updatepanelitemsalepricepanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(updatepanelitemnamepanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
                            .addComponent(updatepanelitempricepanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(updatepanelitemsaletypepanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(removeitemspanel2Layout.createSequentialGroup()
                        .addComponent(removecancelbtn)
                        .addGap(44, 44, 44)
                        .addComponent(removeitembtn)))
                .addContainerGap(123, Short.MAX_VALUE))
        );
        removeitemspanel2Layout.setVerticalGroup(
            removeitemspanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(removeitemspanel2Layout.createSequentialGroup()
                .addComponent(updatepanelitemnamepanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelcompanynamepanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelitempricepanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelitemsalepricepanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelitemquantitypanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatepanelitemsaletypepanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(removeitemspanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(removeitembtn)
                    .addComponent(removecancelbtn))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout removeitemspanelLayout = new javax.swing.GroupLayout(removeitemspanel);
        removeitemspanel.setLayout(removeitemspanelLayout);
        removeitemspanelLayout.setHorizontalGroup(
            removeitemspanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(removeitemspanelLayout.createSequentialGroup()
                .addContainerGap(320, Short.MAX_VALUE)
                .addGroup(removeitemspanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, removeitemspanelLayout.createSequentialGroup()
                        .addComponent(removeitemspanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(50, 50, 50))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, removeitemspanelLayout.createSequentialGroup()
                        .addComponent(updatesearchpanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(164, 164, 164))))
        );
        removeitemspanelLayout.setVerticalGroup(
            removeitemspanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(removeitemspanelLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(updatesearchpanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(removeitemspanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(243, Short.MAX_VALUE))
        );

        centerpanel.add(removeitemspanel, "card2");

        parentpanel.add(centerpanel, java.awt.BorderLayout.CENTER);

        jMenuBar1.setBackground(new java.awt.Color(228, 241, 254));

        systemjmenu1.setText("System");
        jMenuBar1.add(systemjmenu1);

        jmenuhold.setText("On Hold");
        jMenuBar1.add(jmenuhold);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(parentpanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(parentpanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void homelabelFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_homelabelFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_homelabelFocusGained

    private void homelabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_homelabelMouseEntered
        // TODO add your handling code here:
        homelabel.setFont(newfontsize);
        homelabel.setForeground(Color.WHITE); 

        
        
    }//GEN-LAST:event_homelabelMouseEntered

    private void homelabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_homelabelMouseExited
        // TODO add your handling code here:
        homelabel.setFont(fontsize);
        homelabel.setForeground(foregroundcolor);
        homelabel.repaint();
    }//GEN-LAST:event_homelabelMouseExited

    private void homelabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_homelabelMouseClicked
        // TODO add your handling code here:
        centerpanel.removeAll();
        centerpanel.add(homepanel);
        centerpanel.repaint();
        centerpanel.revalidate();
        homestarlabel.setVisible(true);
        addstarlabel.setVisible(false);
        updatestarlabel.setVisible(false);
        removestarlabel.setVisible(false);
        additemspanel2.setVisible(false);
        updateitemspanel1.setVisible(false);
        stockstarlabel.setVisible(false);
        salegraphstarlabel.setVisible(false);
        accountstarlabel.setVisible(false);
    }//GEN-LAST:event_homelabelMouseClicked

    private void additemslabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_additemslabelMouseClicked
        // TODO add your handling code here:
        centerpanel.removeAll();
        centerpanel.add(additems);
        centerpanel.repaint();
        centerpanel.revalidate();
        updateitemspanel1.setVisible(false);
        additemspanel2.setVisible(true);
        
        homestarlabel.setVisible(false);
        addstarlabel.setVisible(true);
        updatestarlabel.setVisible(false);
        removestarlabel.setVisible(false);
        stockstarlabel.setVisible(false);
        salegraphstarlabel.setVisible(false);
        accountstarlabel.setVisible(false);
    }//GEN-LAST:event_additemslabelMouseClicked

    private void updateitemslabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_updateitemslabelMouseClicked
        // TODO add your handling code here:
        String x=JOptionPane.showInputDialog("Enter Password");
        if(x.equals("test")){
        centerpanel.removeAll();
        centerpanel.add(updateitems);
        centerpanel.repaint();
        centerpanel.revalidate();
        billingpanel.setVisible(false);
        updateitemspanel1.setVisible(false);
        additemspanel2.setVisible(false);
        
        homestarlabel.setVisible(false);
        addstarlabel.setVisible(false);
        updatestarlabel.setVisible(true);
        removestarlabel.setVisible(false);
        stockstarlabel.setVisible(false);
        salegraphstarlabel.setVisible(false);
        accountstarlabel.setVisible(false);
            
        }else{
            JOptionPane.showMessageDialog(null, "Wrong password");
        }

    }//GEN-LAST:event_updateitemslabelMouseClicked

    private void removeitemslabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_removeitemslabelMouseClicked
        // TODO add your handling code here:
        centerpanel.removeAll();
        centerpanel.add(removeitemspanel);
        centerpanel.repaint();
        centerpanel.revalidate();
        removeitemspanel2.setVisible(false);
        
        homestarlabel.setVisible(false);
        addstarlabel.setVisible(false);
        updatestarlabel.setVisible(false);
        removestarlabel.setVisible(true);
        stockstarlabel.setVisible(false);
        salegraphstarlabel.setVisible(false);
        accountstarlabel.setVisible(false);
    }//GEN-LAST:event_removeitemslabelMouseClicked

    private void additemslabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_additemslabelMouseEntered
        // TODO add your handling code here:
        additemslabel.setFont(newfontsize);
        additemslabel.setForeground(Color.WHITE);        
    }//GEN-LAST:event_additemslabelMouseEntered

    private void additemslabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_additemslabelMouseExited
        // TODO add your handling code here:
        additemslabel.setFont(fontsize);
        additemslabel.setForeground(foregroundcolor);
    }//GEN-LAST:event_additemslabelMouseExited

    private void updateitemslabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_updateitemslabelMouseEntered
        // TODO add your handling code here:
        updateitemslabel.setFont(newfontsize);
        updateitemslabel.setForeground(Color.WHITE);        
    }//GEN-LAST:event_updateitemslabelMouseEntered

    private void updateitemslabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_updateitemslabelMouseExited
        // TODO add your handling code here:
        updateitemslabel.setFont(fontsize);
        updateitemslabel.setForeground(foregroundcolor);
    }//GEN-LAST:event_updateitemslabelMouseExited

    private void removeitemslabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_removeitemslabelMouseEntered
        // TODO add your handling code here:
        removeitemslabel.setFont(newfontsize);
        removeitemslabel.setForeground(Color.WHITE);        
    }//GEN-LAST:event_removeitemslabelMouseEntered

    private void removeitemslabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_removeitemslabelMouseExited
        // TODO add your handling code here:
        removeitemslabel.setFont(fontsize);
        removeitemslabel.setForeground(foregroundcolor);
    }//GEN-LAST:event_removeitemslabelMouseExited

    private void combosearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_combosearchKeyPressed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null,combosearch.getSelectedItem().toString());
    }//GEN-LAST:event_combosearchKeyPressed

    private void combosearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_combosearchActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_combosearchActionPerformed

    private void updatepanelcancelbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updatepanelcancelbtnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_updatepanelcancelbtnActionPerformed

    private void updatesearchcomboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updatesearchcomboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_updatesearchcomboActionPerformed

    private void updatesearchcomboKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_updatesearchcomboKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_updatesearchcomboKeyPressed

    private void updatepanelupdatebtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updatepanelupdatebtnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_updatepanelupdatebtnActionPerformed

    private void saveadditembtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveadditembtnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_saveadditembtnActionPerformed

    private void canceladditembtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_canceladditembtnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_canceladditembtnActionPerformed

    private void removesearchcomboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removesearchcomboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_removesearchcomboActionPerformed

    private void removesearchcomboKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_removesearchcomboKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_removesearchcomboKeyPressed

    private void removeitembtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeitembtnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_removeitembtnActionPerformed

    private void removecancelbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removecancelbtnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_removecancelbtnActionPerformed

    private void accountlabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_accountlabelMouseClicked
        // TODO add your handling code here:
        centerpanel.removeAll();
        centerpanel.add(accountpanel);
        centerpanel.repaint();
        centerpanel.revalidate();
        
        homestarlabel.setVisible(false);
        addstarlabel.setVisible(false);
        updatestarlabel.setVisible(false);
        removestarlabel.setVisible(false);
        stockstarlabel.setVisible(false);
        salegraphstarlabel.setVisible(false);
        accountstarlabel.setVisible(true);
    }//GEN-LAST:event_accountlabelMouseClicked

    private void accountlabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_accountlabelMouseEntered
        accountlabel.setFont(newfontsize);
        accountlabel.setForeground(Color.WHITE);        // TODO add your handling code here:
    }//GEN-LAST:event_accountlabelMouseEntered

    private void accountlabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_accountlabelMouseExited
        // TODO add your handling code here:
        accountlabel.setFont(fontsize);
        accountlabel.setForeground(foregroundcolor);
    }//GEN-LAST:event_accountlabelMouseExited

    private void stocklabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stocklabelMouseClicked
        // TODO add your handling code here:
        centerpanel.removeAll();
        centerpanel.add(stockpanel);
        centerpanel.repaint();
        centerpanel.revalidate();
        homestarlabel.setVisible(false);
        addstarlabel.setVisible(false);
        updatestarlabel.setVisible(false);
        removestarlabel.setVisible(false);
        stockstarlabel.setVisible(true);
        salegraphstarlabel.setVisible(false);
        accountstarlabel.setVisible(false);
    }//GEN-LAST:event_stocklabelMouseClicked

    private void stocklabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stocklabelMouseEntered
        // TODO add your handling code here:
        stocklabel.setFont(newfontsize);
        stocklabel.setForeground(Color.WHITE);
    }//GEN-LAST:event_stocklabelMouseEntered

    private void stocklabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stocklabelMouseExited
        // TODO add your handling code here:
        stocklabel.setFont(fontsize);
        stocklabel.setForeground(foregroundcolor);
    }//GEN-LAST:event_stocklabelMouseExited

    private void salegraphlabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salegraphlabelMouseClicked
        // TODO add your handling code here:
        //removeitemspanel2.setVisible(false);
        centerpanel.removeAll();
        centerpanel.add(salegraphpanel);
        centerpanel.repaint();
        centerpanel.revalidate();
        homestarlabel.setVisible(false);
        addstarlabel.setVisible(false);
        updatestarlabel.setVisible(false);
        removestarlabel.setVisible(false);
        stockstarlabel.setVisible(false);
        salegraphstarlabel.setVisible(true);
        accountstarlabel.setVisible(false);
    }//GEN-LAST:event_salegraphlabelMouseClicked

    private void salegraphlabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salegraphlabelMouseEntered
        // TODO add your handling code here:
        salegraphlabel.setFont(newfontsize);
        salegraphlabel.setForeground(Color.WHITE);
    }//GEN-LAST:event_salegraphlabelMouseEntered

    private void salegraphlabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salegraphlabelMouseExited
        // TODO add your handling code here:
        salegraphlabel.setFont(fontsize);
        salegraphlabel.setForeground(foregroundcolor);
    }//GEN-LAST:event_salegraphlabelMouseExited

    private void accountsearchcomboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_accountsearchcomboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_accountsearchcomboActionPerformed

    private void accountsearchcomboKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_accountsearchcomboKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_accountsearchcomboKeyPressed
    
    //sales graph start here
    public void printgraphbtnlistener(ActionListener act){
        printgraphbtn.addActionListener(act);
    }
    
    public boolean ismonthradioset(){
        if(monthradio.isSelected()){
            return true;
        }
        return false;
    }
    public boolean isyearradioset(){
        if(yearradio.isSelected()){
            return true;
        }
        return false;
    }
    public String getgraphmonthdate(){
            return ((JTextField)datechooserofmonth.getDateEditor().getUiComponent()).getText();
    }
    public String getgraphyeardate(){
            return ((JTextField)datechooseryear.getDateEditor().getUiComponent()).getText();
    }
    
    public void setsalegraphlist(ArrayList list){
        this.list=list;
        Double d=0.0;
        DefaultCategoryDataset dataset=new DefaultCategoryDataset();
        for(int i=0;i<31;i++){
           d=Double.parseDouble(list.get(i).toString());
           dataset.setValue(d,"Bill In Amount", i+1+"");

        }
        JFreeChart chart=ChartFactory.createBarChart("Bill of Month by day", "Day", "Bill In Amount", dataset,PlotOrientation.VERTICAL,false,true,false);
        CategoryPlot p=chart.getCategoryPlot();
        Color c= new Color(228,241,254);
        p.setRangeGridlinePaint(c);
        ChartFrame frame = new ChartFrame("BAR chart",chart);
        frame.setSize(1000, 600);
        frame.setVisible(true);
    }
    public void setsalegraphyearlist(ArrayList list){
        this.list=list;
        Double d=0.0;
        if(!list.isEmpty()){
        DefaultCategoryDataset dataset=new DefaultCategoryDataset();
        for(int i=0;i<5;i++){
           d=Double.parseDouble(list.get(i).toString());
           dataset.setValue(d,"Bill In Amount", i+"");
           
        }
        JFreeChart chart=ChartFactory.createBarChart("BillING GRAPH of YEAR", "YEARS", "Bill In Amount", dataset,PlotOrientation.VERTICAL,false,true,false);
        CategoryPlot p=chart.getCategoryPlot();
        Color c= new Color(228,241,254);
        p.setRangeGridlinePaint(c);
        ChartFrame frame = new ChartFrame("BAR chart",chart);
        frame.setSize(600, 600);
        frame.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(null,"No Record Found","Aleart",JOptionPane.ERROR);
        }
    }
    
    
    
    //sale graph end here
    
    //stock start here
    public void settotalstock(String stock){
        totalstocktft.setText(stock);
        totalstocktft.setEnabled(false);
    }
    public void showstockbtnlistener(ActionListener act){
      showstockbtn.addActionListener(act);
    }
    public int setalldatavector(ArrayList datavector,DefaultTableModel tm){
       
       tm.setRowCount(0);
       int i=0;
        if(datavector.isEmpty()){
            JOptionPane.showMessageDialog(null,"no Data found");
            return 0;
        }
        Iterator it=datavector.iterator();
        while(it.hasNext()){
            String id=(String) it.next();
            String itemname=(String) it.next();
            String cname=(String) it.next();
            String saleprice=(String) it.next();
            String saletype=(String) it.next();
            String quant=(String) it.next();
            tm.addRow(new Object[]{id,itemname,cname,saleprice,saletype,quant});
            i++;
        }
        return i;
    }
    public void cleartotalstocktabletbn(ActionListener act){
        cleartotalstocktable.addActionListener(act);
    }
    public DefaultTableModel getstocktablemodel(){
       return stockTmodel; 
    }
    public void cleartotalstocktable(DefaultTableModel tm){
        tm.setRowCount(0);
    }
    public void settotalstocktft(int totalitems){
        totalstocktft.setText(totalitems+"");
        totalstocktft.enable(false);
    }
    
    
    
    //stock end here
    
    public void hideadditemspanel(){
       additemspanel2.setVisible(false);
    }
    public void showadditemspanel(){
        additemspanel2.setVisible(true);
    }

    
    
    public void homesearchtabellistener(MouseListener e){
        homesearchtable.addMouseListener( e);
    }
    public DefaultTableModel getDefaulttablemodel(){
        return model;
    }
    public String getitemname(){
     return itemnametft.getText().trim();
    }
    public String getcompanyname(){
        return companynametft.getText().trim();
    }
    public Double getitemprice(){
        return Double.parseDouble(itempricetft.getText().trim());
    }   
    public Double getitemsaleprice(){
        return Double.parseDouble(itemsalepricetft.getText().trim());
    }
    public String getitemsaletype(){
        return itemsaletypecombo.getSelectedItem().toString().trim();
    }
    public String getitemaddeddate(){
        Date date=java.util.Calendar.getInstance().getTime(); 
        DateTimeFormatter df=DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now= LocalDateTime.now();
        return df.format(now);
       
    }
    public int getitemquantity(){
        return Integer.parseInt(itemquantitytft.getText().trim());
    }
    public String getsearcheditem(){
        return combosearch.getSelectedItem().toString().trim();
    }
    public Double getbill(){
        return Double.parseDouble(billtft.getText().trim());
    }
    public int gettotalitempurchased(){
        return Integer.parseInt(totalitemstft.getText().trim());
    }
    
    
    //Event Listnersection start

    public void savebtnlistener(ActionListener act){
        savebtn.addActionListener(act);
    }
    
    public void cancelbtnlistener(ActionListener act){
        cancelbtn.addActionListener(act);
    }
    public void discounttftlistener(KeyListener kl){
        discountallowedtft.addKeyListener(kl);
    }
    public void boton(ActionListener act){
        saveadditembtn.addActionListener(act);
    }
    public void combolistener(KeyListener kl){
        combosearch.getEditor().getEditorComponent().addKeyListener(kl);
    }    
    public void addcancelbtnlistener(ActionListener act){
        canceladditembtn.addActionListener(act);
    }
    public void tablelistener(TableModelListener tl){
        model.addTableModelListener(tl);
        
    }
    public void deleteremovesearchcombolistener(KeyListener kl){
        removesearchcombo.getEditor().getEditorComponent().addKeyListener(kl);
    }
    public void deleteremoveitembtnlistener(ActionListener act){
        removeitembtn.addActionListener(act);
    }
    public void deletecancelitembtnlistener(ActionListener act){
        removecancelbtn.addActionListener(act);
    }        
    public void updatecancelbtnlistener(ActionListener act){
        updatepanelcancelbtn.addActionListener(act);
    }
    public void updatesavebtnlistener(ActionListener act){
        updatepanelupdatebtn.addActionListener(act);
    }
    public void removemenuitemlistener(){
        ActionListener[] act=menuItem.getActionListeners();
        menuItem.removeActionListener(act[0]);
    }    
    public void setmenuitemlistener(ActionListener act){
        menuItem.addActionListener(act);
        
    }
   
//Event Listenersection end

    
//onholdsale start

    public void setonholdmodelontable(Vector datavector){
        JOptionPane.showMessageDialog(null,"msg"+datavector);
        model.addRow(datavector);

    }
    public Vector getsaledatavector(){
        return model.getDataVector();
    }
//onholdsale end    
    //Jmenu functions start
    public void resettable(){
        
        DefaultTableModel m= new DefaultTableModel();
        m=model;
        m.setRowCount(0);
        
    }
    public void addmenuitem(){
        JMenuItem mitem= new JMenuItem("Update");
        menuItem=mitem;
        systemmenu.add(mitem);
        
    }

    public void removemenuitem(){
        jmenuhold.remove(menuItem);
    }

    //jmenu functions end


//Threads section
    public void startthread(){
          
         Thread panelhideshow = new Thread(new Runnable()  
        { 
            @Override
            public void run() { 
                while (true) { 
                     
                    if(homesearchtable.getRowCount()==0){
                        billingpanel.setVisible(false);
                    }
                    else{
                        billingpanel.setVisible(true);

                    }
                } 
            } 
        });
        panelhideshow.start();
    }
    //Threads section ended
    
    public void additemsincombo(ArrayList list){
        combosearch.removeAllItems();
        updatesearchcombo.removeAllItems();
        removesearchcombo.removeAllItems();
        int index=list.size();
        int i=0;
        while(i!=index){
            combosearch.addItem((String) list.get(i));
            updatesearchcombo.addItem((String) list.get(i));
            removesearchcombo.addItem((String) list.get(i));
            i+=1;
        }
        list.clear();
        AutoCompleteDecorator.decorate(combosearch);
        AutoCompleteDecorator.decorate(updatesearchcombo);
        AutoCompleteDecorator.decorate(removesearchcombo);
        
        
    }

    public void settotalitemstft(int items){
        totalitemstft.setText(items+"");
    }
    public void settotalamounttft(Double amount){
        totalamounttft.setText(amount+"");
    }
    public void setdiscountallowedtft(Double discount){
        discountallowedtft.setText(discount+"");
    }
    public void settotalbillft(Double bill){
        billtft.setText(bill+"");
    }
    public void setdiscountallowedtftfocusout(FocusListener f){
        discountallowedtft.addFocusListener(f);
    }
    public Double getdiscountallowed(){
        return Double.parseDouble(discountallowedtft.getText().trim());
    }
    public Double gettotalbill(){
        return Double.parseDouble(totalamounttft.getText().trim());
    }

    public void showinputcancelerror(){
        JOptionPane.showMessageDialog(null,"Process cancelled...");
    }
    public void setid(int id){
        this.id=id;
    }
    public void setitemname(String name){
        this.itemname=name;
    }
    public void resetadditems(){
        itemnametft.setText("");
        companynametft.setText("");
        itemsalepricetft.setText("");
        itempricetft.setText("");
        itemquantitytft.setText("");
    }
    public void setcompanyname(String cname){
        this.companyname=cname;
    }
    public void setitemsaleprice(Double price){
        this.itemsaleprice=price;
    }
    public void setitemsaletype(String type){
        this.itemsaletype=type;
    }
    public void setitemremainingquantity(int quant){
       this.itemquantity=quant;
    }


    public JTable gettable(){

        return homesearchtable;
    }
    public void adddintable(){
        Double weitage=1.0;
        boolean flag=true;
       
             if("KG".equals(itemsaletype)){
                 do{
                    String w=JOptionPane.showInputDialog("Enter Product Weight");

                   try{
                    weitage=Double.parseDouble(w);     
                    itemsaleprice=itemsaleprice*weitage;
                    itemsaleprice=Double.parseDouble(doubleformater.format(itemsaleprice));
                    flag=false;
                   }catch(Exception e){
                       flag=true;
                       JOptionPane.showMessageDialog(null,"Please Enter a Valid Number");
                   }
           }while(flag!=false);
         } 



        Object[] row={id,itemname,companyname,itemsaleprice,itemsaletype,1+""};
        model.addRow(row);
    }
    public DefaultTableModel gettablemodel(){
        return model;
    }
    
    // show errors section start
    public void showshortquantityerror(){
        JOptionPane.showMessageDialog(null,"Remaining Quantity is less than sale quantity");
    }
    public void showdatanotfounderror(){
        JOptionPane.showMessageDialog(null,"Data not found");
    }

    //show errors section end
    
    //Account section start
    public void accountlabellistener(MouseListener ml){
        accountlabel.addMouseListener(ml);
    }
    
    //Account sexction end here
    
    
    //update section start
    
    
    public void updatesearchcomoblistener(KeyListener kl){
        updatesearchcombo.getEditor().getEditorComponent().addKeyListener(kl);
    }
    public String updategetsearchitem(){
        return updatesearchcombo.getSelectedItem().toString().trim();
    }
    public void updatesetitemname(String name){
        updateitemnametft.setText(name);
    }
    public void updatesetcompanyname(String cname){
        updatecompanynametft.setText(cname);
    }
    public void updatesetitemprice(Double price){
        updateitempricetft.setText(price+"");
    }
    public void updatesetitemsaleprice(Double sp){
        updateitemsalepricetft.setText(sp+"");
    }
    public void updatesetitemquantity(int qunt){
        updateitemquantitytft.setText(qunt+"");
    }
    public void updatesetitemsaletype(String type){
        updatecombobox.addItem(type);
    }
    public void showupdatepanel(){
        updateitemspanel1.setVisible(true);
    }
    public void hideudatepanel(){
        updateitemspanel1.setVisible(false);
    }

    public String updategetitemname(){
        return updateitemnametft.getText().trim();
    }
    public String updategetcompanyname(){
        return updatecompanynametft.getText().toString().trim();
    }
    public Double updategetitemprice(){
        return Double.parseDouble(updateitempricetft.getText().trim());
    }
    public Double updategetitemsaleprice(){
        return Double.parseDouble(updateitemsalepricetft.getText().trim());
    }
    public int updategetitemquantity(){
        return Integer.parseInt(updateitemquantitytft.getText().trim());
    }
    public String updategetitemsaletype(){
        return updatecombobox.getSelectedItem().toString().trim();
    }

    //update section ended
   
    //delete items section starts

    public void hideremovepanel(){
        removeitemspanel2.setVisible(false);
    }
    public void showremovepanel(){
        removeitemspanel2.setVisible(true);
    }
    public void deletesetitemname(String name){
        removeitemnametft.setText(name);
    }
    public void deletesetcompanyname(String cname){
        removecompanynametft.setText(cname);
    }
    public void deletesetitemprice(Double price){
        removeitempricetft.setText(price+"");
    }
    public void deletesetitemsaleprice(Double sp){
        removeitemsalepricetft.setText(sp+"");
    }
    public void deletesetitemquantity(int qunt){
        removeitemquantitytft.setText(qunt+"");
    }
    public void deletesetitemsaletype(String type){
        removesaletypecombo.removeAllItems();
        removesaletypecombo.addItem(type);
    }
     public String removegetitemname(){
        return removeitemnametft.getText().trim();
    }
    public String removegetcompanyname(){
        return removecompanynametft.getText().trim();
    }
    public Double removegetitemprice(){
        return Double.parseDouble(removeitempricetft.getText().trim());
    }
    public Double removegetitemsaleprice(){
        return Double.parseDouble(removeitemsalepricetft.getText().trim());
    }
    public int removegetitemquantity(){
        return Integer.parseInt(removeitemquantitytft.getText().trim());
    }
    public String removegetitemsaletype(){
        return removesaletypecombo.getSelectedItem().toString().trim();
    }
    public String removegetsearchitem(){
        return removesearchcombo.getSelectedItem().toString().trim();
    }
    
    //delete section ends here
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DashBoardViewClass.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DashBoardViewClass.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DashBoardViewClass.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DashBoardViewClass.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DashBoardViewClass().setVisible(true);
              
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel accountlabel;
    private javax.swing.JPanel accountpanel;
    private javax.swing.JComboBox<String> accountsearchcombo;
    private javax.swing.JPanel accountsearchpanel;
    private javax.swing.JLabel accountstarlabel;
    private javax.swing.JPanel additems;
    private javax.swing.JLabel additemslabel;
    private javax.swing.JPanel additemspanel;
    private javax.swing.JPanel additemspanel2;
    private javax.swing.JLabel additemspanellabel;
    private javax.swing.JLabel addstarlabel;
    private javax.swing.JPanel billingpanel;
    private javax.swing.JPanel billingtoppanel;
    private javax.swing.JTextField billtft;
    private javax.swing.JButton canceladditembtn;
    private javax.swing.JButton cancelbtn;
    private javax.swing.JPanel centerpanel;
    private javax.swing.JButton cleartotalstocktable;
    private javax.swing.JComboBox<String> combosearch;
    private javax.swing.JTextField companynametft;
    private com.toedter.calendar.JDateChooser datechooserofmonth;
    private com.toedter.calendar.JDateChooser datechooseryear;
    private javax.swing.JTextField discountallowedtft;
    private javax.swing.JPanel graphheaderpanel;
    private javax.swing.JLabel homelabel;
    private javax.swing.JPanel homepanel;
    private javax.swing.JTable homesearchtable;
    private javax.swing.JLabel homestarlabel;
    private javax.swing.JTextField itemnametft;
    private javax.swing.JTextField itempricetft;
    private javax.swing.JTextField itemquantitytft;
    private javax.swing.JTextField itemsalepricetft;
    private javax.swing.JComboBox<String> itemsaletypecombo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JMenu jmenuhold;
    private javax.swing.JPanel leftpanel;
    private javax.swing.JRadioButton monthradio;
    private javax.swing.JPanel parentpanel;
    private javax.swing.JLabel poslabel;
    private javax.swing.JButton printgraphbtn;
    private javax.swing.JLabel proprietername;
    private javax.swing.JButton removecancelbtn;
    private javax.swing.JTextField removecompanynametft;
    private javax.swing.JButton removeitembtn;
    private javax.swing.JTextField removeitemnametft;
    private javax.swing.JTextField removeitempricetft;
    private javax.swing.JTextField removeitemquantitytft;
    private javax.swing.JTextField removeitemsalepricetft;
    private javax.swing.JLabel removeitemslabel;
    private javax.swing.JPanel removeitemspanel;
    private javax.swing.JPanel removeitemspanel2;
    private javax.swing.JComboBox<String> removesaletypecombo;
    private javax.swing.JComboBox<String> removesearchcombo;
    private javax.swing.JLabel removestarlabel;
    private javax.swing.JPanel rightpanel;
    private javax.swing.JPanel salegraphheaderpanel;
    private javax.swing.JLabel salegraphlabel;
    private javax.swing.JPanel salegraphpanel;
    private javax.swing.JLabel salegraphstarlabel;
    private javax.swing.JButton saveadditembtn;
    private javax.swing.JButton savebtn;
    private javax.swing.JPanel shopnamepanel;
    private javax.swing.JButton showstockbtn;
    private javax.swing.JPanel stockheaderpanel;
    private javax.swing.JLabel stocklabel;
    private javax.swing.JPanel stockpanel;
    private javax.swing.JLabel stockstarlabel;
    private javax.swing.JTable stocktable;
    private javax.swing.JMenu systemjmenu1;
    private javax.swing.JTextField totalamounttft;
    private javax.swing.JTextField totalitemstft;
    private javax.swing.JTextField totalstocktft;
    private javax.swing.JComboBox<String> updatecombobox;
    private javax.swing.JLabel updatecompanynamepanel;
    private javax.swing.JLabel updatecompanynamepanel1;
    private javax.swing.JLabel updatecompanynamepanel2;
    private javax.swing.JTextField updatecompanynametft;
    private javax.swing.JLabel updateitemnamelabel;
    private javax.swing.JLabel updateitemnamelabel1;
    private javax.swing.JLabel updateitemnamelabel2;
    private javax.swing.JTextField updateitemnametft;
    private javax.swing.JLabel updateitempricelabel;
    private javax.swing.JLabel updateitempricelabel1;
    private javax.swing.JLabel updateitempricelabel2;
    private javax.swing.JTextField updateitempricetft;
    private javax.swing.JLabel updateitemquantitylabel;
    private javax.swing.JLabel updateitemquantitylabel1;
    private javax.swing.JLabel updateitemquantitylabel2;
    private javax.swing.JTextField updateitemquantitytft;
    private javax.swing.JPanel updateitems;
    private javax.swing.JLabel updateitemsalepricelabel;
    private javax.swing.JLabel updateitemsalepricelabel1;
    private javax.swing.JLabel updateitemsalepricelabel2;
    private javax.swing.JTextField updateitemsalepricetft;
    private javax.swing.JLabel updateitemslabel;
    private javax.swing.JPanel updateitemspanel1;
    private javax.swing.JButton updatepanelcancelbtn;
    private javax.swing.JPanel updatepanelcompanynamepanel;
    private javax.swing.JPanel updatepanelcompanynamepanel1;
    private javax.swing.JPanel updatepanelcompanynamepanel2;
    private javax.swing.JPanel updatepanelitemnamepanel;
    private javax.swing.JPanel updatepanelitemnamepanel1;
    private javax.swing.JPanel updatepanelitemnamepanel2;
    private javax.swing.JPanel updatepanelitempricepanel;
    private javax.swing.JPanel updatepanelitempricepanel1;
    private javax.swing.JPanel updatepanelitempricepanel2;
    private javax.swing.JPanel updatepanelitemquantitypanel;
    private javax.swing.JPanel updatepanelitemquantitypanel1;
    private javax.swing.JPanel updatepanelitemquantitypanel2;
    private javax.swing.JPanel updatepanelitemsalepricepanel;
    private javax.swing.JPanel updatepanelitemsalepricepanel1;
    private javax.swing.JPanel updatepanelitemsalepricepanel2;
    private javax.swing.JPanel updatepanelitemsaletypepanel;
    private javax.swing.JPanel updatepanelitemsaletypepanel1;
    private javax.swing.JPanel updatepanelitemsaletypepanel2;
    private javax.swing.JButton updatepanelupdatebtn;
    private javax.swing.JComboBox<String> updatesearchcombo;
    private javax.swing.JPanel updatesearchpanel;
    private javax.swing.JPanel updatesearchpanel1;
    private javax.swing.JLabel updatestarlabel;
    private javax.swing.JRadioButton yearradio;
    // End of variables declaration//GEN-END:variables

    
}
