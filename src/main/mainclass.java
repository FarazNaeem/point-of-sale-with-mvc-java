/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javax.swing.JOptionPane;

/**
 *
 * @author Faraz Naeem
 */
public class mainclass {
    public static void main(String args[]){
        try{
                modelclasses.connection con= new modelclasses.connection();
                modelclasses.databasemodel model= new modelclasses.databasemodel(con);
            //    modelclasses.salesmodel salesmodel= new modelclasses.salesmodel(con);
                modelclasses.updateitemsmodel updatemodel=new modelclasses.updateitemsmodel(con);
                modelclasses.deleteitemsmodel deletemodel=new modelclasses.deleteitemsmodel(con);
                modelclasses.loginmodel loginmodel=new modelclasses.loginmodel(con);
                modelclasses.salesgraphmodel salegraphmodel= new modelclasses.salesgraphmodel(con);
                modelclasses.stockmodel stockmodel= new modelclasses.stockmodel(con);
                
                viewclasses.product prod= new viewclasses.product();
                viewclasses.DashBoardViewClass view= new viewclasses.DashBoardViewClass();
                viewclasses.login loginview= new viewclasses.login();
                
                controllerclasses.databasecontroller controller= new controllerclasses.databasecontroller(model,view,prod);            
                controllerclasses.searchcontroller search= new controllerclasses.searchcontroller(view,model);
                controllerclasses.updateitemscontroller updatecontroller= new controllerclasses.updateitemscontroller(updatemodel,view);
                controllerclasses.deleteitemcontroller deletecontroller= new controllerclasses.deleteitemcontroller(deletemodel,view);
                controllerclasses.logincontroller logincontroller= new controllerclasses.logincontroller(loginview,loginmodel,view);
                controllerclasses.salesgraphcontroller graphcontroller= new controllerclasses.salesgraphcontroller(view,salegraphmodel);
                controllerclasses.stockcontroller stockcontroller= new controllerclasses.stockcontroller(view,stockmodel);
//loginview.setVisible(true);
                view.setVisible(true);

        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Database Not Found " +ex);
        }
        
    }
    
}
